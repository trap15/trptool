OBJECTS	?=
OBJECTS	+= \
	src/main.o \
		src/format.o \
		src/pcx.o \
		src/pcg.o \
		src/misc.o \
		src/wav.o \
		src/lze.o \
	\
	src/cmd_fspack.o \
	src/cmd_mkpcg.o \
	src/cmd_ne2bin.o \
	src/cmd_wavconv.o \
	src/cmd_wwcodec.o \
	src/cmd_wshead.o \
	src/cmd_pcxexp.o \
	src/cmd_pcxflip.o \
	src/cmd_pcxmerge.o \
	src/cmd_pcxatlas.o \
	src/cmd_pcx2tex.o \
	src/cmd_dat4pcx.o \
	src/cmd_pcx2bin.o \
	src/cmd_romsplit.o \
	src/cmd_bin2asm.o \
	src/cmd_pcx2spx.o \
	src/cmd_satpack.o \
	src/cmd_deslash.o \
	src/cmd_fontmk.o \
	src/cmd_palext.o

OUTPUT	?= trptool
CFLAGS  ?=
CFLAGS	+= -Isrc -std=gnu11 -Wall -Wextra -pedantic
ifeq ($(DEBUG),1)
CFLAGS	+= -DDEBUG=1 -g
endif
LDFLAGS	?= 
CC	?= gcc
RM	?= rm
INSTALL_PATH	?= /usr/local

.PHONY: all clean install uninstall

all: $(OBJECTS) $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $+
clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)
install:
	install	$(OUTPUT) $(INSTALL_PATH)/bin
uninstall:
	rm -i $(INSTALL_PATH)/bin/$(OUTPUT)

src/main.o: src/commands.def
src/format.o: src/gamedef.inc.c src/hwdef.inc.c
