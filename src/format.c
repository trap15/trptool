#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "format.h"

static HardwareDef s_hardwares[] = {
#include "hwdef.inc.c"
  {.name=""},
};

static GameDef s_games[] = {
#include "gamedef.inc.c"
  { .name = "none",
    .hwdef_name = "none",
  },
  {.name=""},
};

static HardwareDef s_curhw;
static GameDef s_curgame;

static HardwareDef *prv_hw_get_raw(const char *name) {
  for(HardwareDef *hw = s_hardwares; hw->name[0] != '\0'; hw++) {
    if(strcmp(name, hw->name) != 0)
      continue;
    return hw;
  }
  return NULL;
}

static GameDef *prv_game_get_raw(const char *name) {
  for(GameDef *game = s_games; game->name[0] != '\0'; game++) {
    if(strcmp(name, game->name) != 0)
      continue;
    return game;
  }
  return NULL;
}

static HardwareDef *prv_hw_recurse(HardwareDef *tgt, const char *name) {
  HardwareDef *hw = prv_hw_get_raw(name);
  if(hw == NULL)
    return NULL;

  if(hw->inherit_name) {
    if(prv_hw_recurse(tgt, hw->inherit_name) == NULL)
      return NULL;
    tgt->name = hw->name;
#define COND_COPY(dst,src, var,trig) if(src->var != trig) dst->var = src->var
    COND_COPY(tgt,hw,pcg_bpp,0);
    COND_COPY(tgt,hw,pcg_w,0);
    COND_COPY(tgt,hw,pcg_h,0);
    COND_COPY(tgt,hw,pcg_planar,0);
    COND_COPY(tgt,hw,pcg_bit_flip,0);
    COND_COPY(tgt,hw,pcg_pix_flip,0);
    COND_COPY(tgt,hw,pal_size,0);
    COND_COPY(tgt,hw,pal_count,0);
    COND_COPY(tgt,hw,pal_depth,0);
    COND_COPY(tgt,hw,pal_r_size,-1);
    COND_COPY(tgt,hw,pal_r_shift,-1);
    COND_COPY(tgt,hw,pal_g_size,-1);
    COND_COPY(tgt,hw,pal_g_shift,-1);
    COND_COPY(tgt,hw,pal_b_size,-1);
    COND_COPY(tgt,hw,pal_b_shift,-1);
    COND_COPY(tgt,hw,map_neutral_bit,0);
    COND_COPY(tgt,hw,map_xflip_bit,0);
    COND_COPY(tgt,hw,map_yflip_bit,0);
    COND_COPY(tgt,hw,map_pal_bit,0);
    COND_COPY(tgt,hw,map_emptyval,-1);
    COND_COPY(tgt,hw,map_rotate,0);
  }else{
    *tgt = *hw;
  }
  return tgt;
}

HardwareDef *hw_get(const char *name) {
  return prv_hw_recurse(&s_curhw, name);
}

// ex. `firelancer`, `firelancer-ws2bpp-color`, `none-ws4bpp`
GameDef *fmt_get(const char *name) {
  char *namedup = strdup(name);
  char *hwstr = strchr(namedup, '-');
  if(hwstr != NULL) {
    *hwstr = '\0';
    hwstr++;
  }
  GameDef *game = prv_game_get_raw(namedup);
  free(namedup);
  if(game == NULL)
    return NULL;

  s_curgame = *game;
  if(hwstr == NULL)
    hwstr = (char*)s_curgame.hwdef_name;
  s_curgame.hwdef = hw_get(hwstr);
  game = &s_curgame;
  return game;
}

// assumes host is little endian
uint16_t endian16(GameDef *game, uint16_t val) {
  int need_swap = 0;
  if(game == NULL || game->hwdef->endian > 0) need_swap = 1;

  if(need_swap) {
    val = (val >> 8) | (val << 8);
  }
  return val;
}
uint32_t endian32(GameDef *game, uint32_t val) {
  int need_swap = 0;
  if(game == NULL || game->hwdef->endian > 0) need_swap = 1;

  if(need_swap) {
    val = ((val >>24) & 0x000000FF) |
          ((val >> 8) & 0x0000FF00) |
          ((val << 8) & 0x00FF0000) |
          ((val <<24) & 0xFF000000);
  }
  return val;
}
