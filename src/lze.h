#ifndef LZE_H_
#define LZE_H_

size_t lze_encode(uint8_t *dest, const uint8_t *src, size_t src_size);
size_t lze_decode(uint8_t *dest, const uint8_t *src, size_t src_size);

#endif
