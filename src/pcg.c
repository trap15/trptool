#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"
#include "pcg.h"

static size_t prv_stride(GameDef *fmt) {
  return fmt->hwdef->pcg_w * fmt->hwdef->pcg_bpp / 8;
}

static void prv_flip_fmt(size_t *x, size_t *y, GameDef *fmt, int flipx, int flipy, int rot) {
  flip_perform(x,y, fmt->hwdef->pcg_w,fmt->hwdef->pcg_h, flipx, flipy, rot);
}

static uint32_t prv_build_line_planar(void *tile, GameDef *fmt, PcxFile *pcx, size_t y,
                                      size_t src_x, size_t src_y,
                                      int flipx, int flipy, int rot) {
  uint32_t pal = UINT32_MAX;
  size_t stride = prv_stride(fmt);
  for(size_t bit = 0; bit < stride; bit++) {
    size_t v;
    for(size_t x = 0; x < fmt->hwdef->pcg_w; x++) {
      size_t px = x, py = y;
      prv_flip_fmt(&px,&py,fmt,flipx,flipy,rot);

      uint8_t col = pcx->data[(src_x + px) + ((src_y + py) * pcx->w)];
      uint8_t pixpal = col >> fmt->hwdef->pcg_bpp;
      if(pal == UINT32_MAX) pal = pixpal;
      if(pal != pixpal) {
        fprintf(stderr, "Tile palette error!\n");
      }
      col &= (1 << fmt->hwdef->pcg_bpp) - 1;

      col = (col >> bit) & 1;
      if(fmt->hwdef->pcg_pix_flip > 0) {
        v <<= 1;
        v |= col;
      }else{
        v |= col << 8;
        v >>= 1;
      }
    }
    if(fmt->hwdef->pcg_bit_flip > 0) {
      ((uint8_t*)tile)[stride - bit - 1] = v;
    }else{
      ((uint8_t*)tile)[bit] = v;
    }
  }
  return pal;
}

static uint32_t prv_build_line_chunky(void *tile, GameDef *fmt, PcxFile *pcx, size_t y,
                                      size_t src_x, size_t src_y,
                                      int flipx, int flipy, int rot) {
  uint32_t pal = UINT32_MAX;
  size_t stride = prv_stride(fmt);
  size_t pix_per_byte = 8 / fmt->hwdef->pcg_bpp;
  if(fmt->hwdef->pcg_xflip > 0)
    flipx = !flipx;
  if(fmt->hwdef->pcg_yflip > 0)
    flipy = !flipy;
  for(size_t x = 0; x < stride; x++) {
    size_t v = 0;

    for(size_t bit = 0; bit < pix_per_byte; bit++) {
      size_t px = x*pix_per_byte + bit;
      size_t py = y;
      prv_flip_fmt(&px,&py,fmt,flipx,flipy,rot);

      uint8_t col = pcx->data[(src_x + px) + ((src_y + py) * pcx->w)];
      uint8_t pixpal = col >> fmt->hwdef->pcg_bpp;
      if(pal == UINT32_MAX) pal = pixpal;
      if(pal != pixpal) {
          fprintf(stderr, "Tile palette error!\n");
      }
      col &= (1 << fmt->hwdef->pcg_bpp) - 1;
      if(fmt->hwdef->pcg_bit_flip > 0) {
        fprintf(stderr, "Bit flip in chunky NYS\n");
      }

      if(fmt->hwdef->pcg_pix_flip > 0) {
        v <<= fmt->hwdef->pcg_bpp;
        v |= col;
      }else{
        v |= col << 8;
        v >>= fmt->hwdef->pcg_bpp;
      }
    }
    ((uint8_t*)tile)[x] = v;
  }
  return pal;
}

uint32_t pcg_build_tile(void *tile, GameDef *fmt, PcxFile *pcx, size_t src_x, size_t src_y,
                        int flipx, int flipy, int rot) {
  uint8_t *ptr = tile;
  uint32_t pal = 0;
  for(size_t y = 0; y < fmt->hwdef->pcg_h; y++) {
    if(fmt->hwdef->pcg_planar > 0)
      pal = prv_build_line_planar(ptr, fmt, pcx, y, src_x, src_y, flipx, flipy, rot);
    else
      pal = prv_build_line_chunky(ptr, fmt, pcx, y, src_x, src_y, flipx, flipy, rot);
    ptr += prv_stride(fmt);
  }
  return pal;
}

size_t pcg_tile_size(GameDef *fmt) {
  return prv_stride(fmt) * fmt->hwdef->pcg_h;
}

void flip_perform(size_t *x, size_t *y, size_t max_w, size_t max_h,
                  int flipx, int flipy, int rot) {
  if(flipx) *x = max_w - *x - 1;
  if(flipy) *y = max_h - *y - 1;
  if(rot) {
    size_t tmp = *x;
    *x = *y;
    *y = tmp;
  }
}
