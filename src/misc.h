#ifndef MISC_H_
#define MISC_H_

#define errprint(...) fprintf(stderr, __VA_ARGS__)

#if DEBUG
# define dbgprint(...) fprintf(stderr, __VA_ARGS__)
#else
# define dbgprint(...)
#endif

uint32_t hash32(const uint8_t *data, size_t len);
int align(int alignment, int value);

#endif
