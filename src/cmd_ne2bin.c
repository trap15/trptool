#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"

uint8_t r8(FILE *fp) {
  uint8_t v;
  fread(&v, 1, 1, fp);
  return v;
}
uint16_t r16(FILE *fp) {
  uint16_t v;
  v = r8(fp);
  v |= r8(fp) << 8;
  return v;
}
uint32_t r32(FILE *fp) {
  uint32_t v;
  v = r16(fp);
  v |= r16(fp) << 16;
  return v;
}

#define MAX_RELOCS 0x10
#define MAX_SEGMENTS 0x10

typedef struct __attribute__((packed)) {
  uint8_t addr_type;
  uint8_t reloc_type;
  uint16_t offs;
  union {
    uint8_t extra[4];
    struct {
      uint16_t reftbl;
      uint16_t funcord;
    } import_ord;
    struct {
      uint16_t reftbl;
      uint16_t nameoffs;
    } import_name;
    struct {
      uint8_t segnum;
      uint8_t zero;
      uint16_t segoff;
    } internal_ref;
  };
} NeRelocation;

typedef struct __attribute__((packed)) {
  uint16_t data_ofs;
  uint16_t data_len;
  uint16_t flag;
  uint16_t alloc_size;

  uint16_t seg_num;
  uint16_t virt_addr;

  void *data;
  uint16_t reloc_count;
  NeRelocation relocs[MAX_RELOCS];
} NeSegment;

static void prv_apply_reloc(NeSegment *segs, NeSegment *seg, NeRelocation *reloc) {
  if(reloc->reloc_type != 0) {
    errprint("Only support reloc_type 0 (is %d)\n", reloc->reloc_type);
    return;
  }
  if(reloc->addr_type != 2) {
    errprint("Only support addr_type 2 (is %d)\n", reloc->addr_type);
    return;
  }
  uint16_t offs = reloc->offs;
  uint8_t *data = (uint8_t*)seg->data;
  uint16_t newvalue = segs[reloc->internal_ref.segnum - 1].virt_addr;
  while(offs != 0xFFFF) {
    uint16_t nextoffs = *(uint16_t*)(data+offs);
    dbgprint("Reloc %X:%X -> %X\n", seg->seg_num, offs, newvalue);
    *(uint16_t*)(data+offs) = newvalue;
    offs = nextoffs;
  }
}

typedef struct {
  int32_t vma;
  uint32_t offset;
  int32_t length;
  const char *outfn;
  int segidx;
} SegmentInfo;

static void prv_write_segment(SegmentInfo *seginfo, NeSegment *seg) {
  const char *fn = seginfo->outfn;
  if(fn == NULL) return;
  uint8_t *data = (uint8_t*)seg->data;
  if(seginfo->length < 0)
    seginfo->length = seg->data_len - seginfo->offset;
  data += seginfo->offset;
  FILE *ofp = fopen(fn, "ab");
  fwrite(data, seginfo->length, 1, ofp);
  fclose(ofp);
}

static void prv_clear_file(const char *fn) {
  if(fn == NULL) return;
  fclose(fopen(fn, "wb"));
}

#define MAX_CODESEG 4
#define MAX_DATASEG 4

int cmd_ne2bin(int argc, const char *argv[]) {
  const char *arg_inexe = NULL;

  int dst_idx = 0;

  int dataseg_count = -1;
  SegmentInfo datasegs[MAX_DATASEG];
  int codeseg_count = -1;
  SegmentInfo codesegs[MAX_CODESEG];

  for(int l = 0; l < 2; l++) {
    SegmentInfo *seg;
    int i;
    if(l == 0) {
      i = MAX_DATASEG;
      seg = datasegs;
    }else{
      i = MAX_CODESEG;
      seg = codesegs;
    }
    for(i--; i >= 0; i--, seg++) {
      seg->outfn = NULL;
      seg->segidx = 0;
      seg->vma = -1;
      seg->offset = 0;
      seg->length = -1;
    }
  }

  for(int i = 0; i < argc; i++) {
    if(argv[i][0] != '-') {
      int cs_idx = dst_idx - 1;
      int ds_idx = cs_idx - codeseg_count - 1;
      dbgprint("%d:%d:%d %d:%d\n", dst_idx, cs_idx, ds_idx, codeseg_count, dataseg_count);
      if(dst_idx == 0) {
        arg_inexe = argv[i];
      }else if(cs_idx < codeseg_count+1) {
        codesegs[cs_idx].outfn = argv[i];
      }else if(ds_idx < dataseg_count+1) {
        datasegs[ds_idx].outfn = argv[i];
      }
      dst_idx++;
      continue;
    }
    SegmentInfo *seg;
    int idx = strtol(argv[i]+3, NULL, 0);
    if(argv[i][3] == '\0')
      idx = 0;
    if(argv[i][2] == 'c') {
      seg = &codesegs[idx];
      if(idx > codeseg_count) codeseg_count = idx;
    }else if(argv[i][2] == 'd') {
      seg = &datasegs[idx];
      if(idx > dataseg_count) dataseg_count = idx;
    }

    switch(argv[i][1]) {
      case 'o':
        seg->outfn = argv[++i];
        break;
      case 'a':
        seg->vma = strtol(argv[++i], NULL, 0);
        break;
      case 'p':
        seg->offset = strtol(argv[++i], NULL, 0);
        break;
      case 'l':
        seg->length = strtol(argv[++i], NULL, 0);
        break;
    }
  }
  FILE *ifp = fopen(arg_inexe, "rb");

  fseek(ifp, 0x3C, SEEK_SET);
  uint32_t base = r32(ifp);
  fseek(ifp, base, SEEK_SET);
  // Now at NE header.
  fseek(ifp, base+0x1C, SEEK_SET);
  uint16_t segment_count = r16(ifp);

  fseek(ifp, base+0x22, SEEK_SET);
  uint16_t segtbl_ofs = r16(ifp);

  // i like th efixd buffrs
  NeSegment segs[MAX_SEGMENTS];

  fseek(ifp, base+segtbl_ofs, SEEK_SET);
  for(uint16_t i = 0; i < segment_count; i++) {
    fread(&segs[i], 8, 1, ifp);
    segs[i].seg_num = i + 1;
  }

  int32_t codeseg_idx = 0, dataseg_idx = 0;
  for(uint16_t i = 0; i < segment_count; i++) {
    if(segs[i].data_ofs == 0)
      continue;
    dbgprint("Segment %d:\n", i);
    fseek(ifp, segs[i].data_ofs << 4, SEEK_SET);
    segs[i].data = malloc(segs[i].data_len);
    fread(segs[i].data, segs[i].data_len, 1, ifp);
    segs[i].reloc_count = r16(ifp);
    if(segs[i].flag & 1) {
      segs[i].virt_addr = datasegs[dataseg_idx].vma;
      datasegs[dataseg_idx++].segidx = i;
    }else{
      segs[i].virt_addr = codesegs[codeseg_idx].vma;
      if(segs[i].virt_addr == (uint16_t)-1) {
        segs[i].virt_addr = codesegs[codeseg_idx-1].vma + ((segs[codesegs[codeseg_idx - 1].segidx].data_len + 0xF) >> 4);
      }

      codesegs[codeseg_idx++].segidx = i;
    }
    dbgprint("  vma: $%X\n", segs[i].virt_addr);
    dbgprint("  data_ofs: $%X\n", segs[i].data_ofs);
    dbgprint("  data_len: $%X\n", segs[i].data_len);
    dbgprint("  flag: $%X\n", segs[i].flag);
    dbgprint("  reloc_count: $%X\n", segs[i].reloc_count);
    for(uint16_t l = 0; l < segs[i].reloc_count; l++) {
      segs[i].relocs[l].addr_type = r8(ifp);
      segs[i].relocs[l].reloc_type = r8(ifp);
      segs[i].relocs[l].offs = r16(ifp);
      fread(segs[i].relocs[l].extra, 4, 1, ifp);
      dbgprint("    [%d] = {\n", l);
      dbgprint("      addr_type: $%X\n", segs[i].relocs[l].addr_type);
      dbgprint("      reloc_type: $%X\n", segs[i].relocs[l].reloc_type);
      dbgprint("      offs: $%X\n", segs[i].relocs[l].offs);
      dbgprint("      extra: %02X %02X %02X %02X\n", segs[i].relocs[l].extra[0],
               segs[i].relocs[l].extra[1], segs[i].relocs[l].extra[2], segs[i].relocs[l].extra[3]);
      dbgprint("    }\n");
    }
  }
  fclose(ifp);

  for(uint16_t i = 0; i < segment_count; i++) {
    if(segs[i].data_ofs == 0)
      continue;
    for(uint16_t l = 0; l < segs[i].reloc_count; l++) {
      prv_apply_reloc(segs, &segs[i], &segs[i].relocs[l]);
    }
  }

  for(int i = 0; i < dataseg_idx; i++) {
    prv_clear_file(datasegs[i].outfn);
  }
  for(int i = 0; i < codeseg_idx; i++) {
    prv_clear_file(codesegs[i].outfn);
  }

  for(int i = 0; i < dataseg_idx; i++) {
    prv_write_segment(&datasegs[i], &segs[datasegs[i].segidx]);
  }
  for(int i = 0; i < codeseg_idx; i++) {
    prv_write_segment(&codesegs[i], &segs[codesegs[i].segidx]);
  }

  return 0;
}
