#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "format.h"

//format outfolder in1.bin [in2.bin [in3.bin [...]]]

static void prv_do_rom(const char *folder, const char *infn, RomSplit *rom) {
  FILE *fp = fopen(infn, "rb");
  size_t fsize;
  fseek(fp, 0, SEEK_END);
  fsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  uint8_t *fdata = malloc(fsize);
  fread(fdata, fsize, 1, fp);
  fclose(fp);

  char outname[256];
  uint8_t tmpbuf[16];
  for(int i = 0; rom->outputs[i].name != NULL; i++) {
    sprintf(outname, "%s/%s", folder, rom->outputs[i].name);
    fp = fopen(outname, "wb");
    size_t rompos = rom->outputs[i].offset;
    for(size_t pos = 0; pos < rom->outputs[i].size; pos++) {
      size_t rpos = rompos ^ rom->outputs[i].addr_xor;
      for(size_t byte = 0; byte < rom->outputs[i].wordsize; byte++) {
        if(rpos >= fsize)
          tmpbuf[byte] = 0xFF;
        else
          tmpbuf[byte] = fdata[rpos];
        rpos++;
      }
      fwrite(tmpbuf, rom->outputs[i].wordsize, 1, fp);
      rompos += rom->outputs[i].wordsize * (rom->outputs[i].wordskip+1);
    }
    fclose(fp);
  }

  free(fdata);
}

int cmd_romsplit(int argc, const char *argv[]) {
  const char *arg_fmt = argv[0];
  const char *arg_folder = argv[1];
  const char **arg_input = argv+2;
  GameDef *game = fmt_get(arg_fmt);
  if(game == NULL) {
    errprint("bad fmt %s\n", arg_fmt);
    return 1;
  }

  for(int i = 0; i < argc-2; i++) {
    if(game->hwdef->roms[i].outputs[0].name == NULL)
      continue;
    prv_do_rom(arg_folder, arg_input[i], &game->hwdef->roms[i]);
  }

  return 0;
}
