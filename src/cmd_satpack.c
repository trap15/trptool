#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "misc.h"
#include "format.h"

#define MAX_FSENT 256
#define MAX_DATA_LENGTH 256
#define MAX_LINE_LENGTH 256

typedef struct {
  uint16_t id;
  char name[MAX_DATA_LENGTH];
  char srcfn[MAX_DATA_LENGTH];
  uint32_t flags;

  size_t rawsize;
  void *rawdata;
  size_t size;
  void *data;
} FsEnt;

typedef struct __attribute__((packed)) {
  uint32_t offset;
  uint32_t size;
  uint32_t flag;
  uint32_t rawsize;
} FsTblEnt;

static FsEnt s_fsents[MAX_FSENT];
static size_t s_fsent_count;
static char s_packname[MAX_DATA_LENGTH];

#define FLAG_COMPRESSED 0x100

#define AREA_WRAMH 0x00
#define AREA_WRAML 0x01
#define AREA_CRAM 0x10
#define AREA_VDP1 0x20
#define AREA_VDP2 0x30
#define AREA_VDP2_NBG0 (AREA_VDP2 | 1)
#define AREA_VDP2_NBG1 (AREA_VDP2 | 2)
#define AREA_VDP2_NBG2 (AREA_VDP2 | 3)

static uint32_t prv_area2flag(const char *str) {
  uint32_t flags = 0;
  if(strcmp(str, "WRAMH") == 0) {
    flags |= AREA_WRAMH;
  }else if(strcmp(str, "WRAML") == 0) {
    flags |= AREA_WRAML;
  }else if(strcmp(str, "CRAM") == 0) {
    flags |= AREA_CRAM;
  }else if(strcmp(str, "VDP1") == 0) {
    flags |= AREA_VDP1;
  }else if(strcmp(str, "VDP2") == 0) {
    flags |= AREA_VDP2;
  }else if(strcmp(str, "VDP2_NBG0") == 0) {
    flags |= AREA_VDP2_NBG0;
  }else if(strcmp(str, "VDP2_NBG1") == 0) {
    flags |= AREA_VDP2_NBG1;
  }else if(strcmp(str, "VDP2_NBG2") == 0) {
    flags |= AREA_VDP2_NBG2;
  }
  return flags;
}

static void prv_check_id(size_t max_id) {
  for(size_t i = 0; i < s_fsent_count; i++) {
    if(max_id == s_fsents[i].id) {
      errprint("Duplicate ID %zu\n", max_id);
    }
  }
}

static int prv_handle_section(char *key, size_t *max_id) {
  char *val;
  int was_file = 0;
  val = strchr(key, '=');
  if(val == NULL)
    return 0;
  *(val++) = '\0';

  if(strcmp(key, "ID") == 0) {
    *max_id = strtol(val, NULL, 0);
    prv_check_id(*max_id);
  }else if(strcmp(key, "PACK") == 0) {
    strcpy(s_packname, val);
    strcat(s_packname, "_");
  }else if(strcmp(key, "NAME") == 0) {
    strncpy(s_fsents[s_fsent_count].name, val, MAX_DATA_LENGTH);
    was_file = 1;
  }else if(strcmp(key, "SOURCE") == 0) {
    strncpy(s_fsents[s_fsent_count].srcfn, val, MAX_DATA_LENGTH);
    was_file = 1;
  }else if(strcmp(key, "AREA") == 0) {
    s_fsents[s_fsent_count].flags |= prv_area2flag(val);
    was_file = 1;
  }
  return was_file;
}

static int prv_handle_line(char *str, size_t *max_id) {
  char *tok;
  int was_file = 0;
  do {
    tok = strtok(str, ";");
    str = NULL;
    if(tok == NULL)
      break;
    if(tok[0] == '\0')
      break;
    // Strip trailing spaces
    char *ptr = tok + strlen(tok);
    while(*ptr == ' ' && ptr != tok) {
      *ptr = '\0';
      ptr--;
    }
    // Strip prefaced spaces
    while(*tok == ' ') tok++;
    if(prv_handle_section(tok, max_id))
      was_file = 1;
  } while(tok != NULL);
  return was_file;
}

static void prv_write_timestamp(char *str) {
  time_t t = time(NULL);
  struct tm ti;
  localtime_r(&t, &ti);
  strftime(str+0x00, 0x10, "%Y.%m.%d", &ti);
  strftime(str+0x10, 0x10, "%H:%M:%S", &ti);
}

int cmd_satpack(int argc, const char *argv[]) {
  (void)argc;
  FILE *ifp = fopen(argv[0], "r");

  s_packname[0] = '\0';

  char line[MAX_LINE_LENGTH];

  size_t max_id = 0;
  memset(s_fsents, 0, sizeof(s_fsents));
  s_fsent_count = 0;
  while(!feof(ifp)) {
    line[0] = '\0';
    fgets(line, MAX_LINE_LENGTH, ifp);
    char *s = strchr(line, '#');
    if(s != NULL) *s = '\0';
    s = strchr(line, '\r');
    if(s != NULL) *s = '\0';
    s = strchr(line, '\n');
    if(s != NULL) *s = '\0';

    if(line[0] == '\0')
      continue;
    if(prv_handle_line(line, &max_id)) {
      s_fsents[s_fsent_count].id = max_id;
      max_id++;
      s_fsent_count++;
      s_fsents[s_fsent_count].id = max_id;
    }
  }

  fclose(ifp);

  FILE *hdrfp = fopen(argv[2], "wb");
  fprintf(hdrfp, "#ifndef RSRC__%sH_\n", s_packname);
  fprintf(hdrfp, "#define RSRC__%sH_\n\n", s_packname);

  size_t pack_size = 0;
  for(size_t i = 0; i < s_fsent_count; i++) {
    FsEnt *ent = &s_fsents[i];
    if(ent->id > max_id)
      max_id = ent->id;
    // Dump rsrcid
    fprintf(hdrfp, "#define RID_%s%s (0x%03X)\n", s_packname, ent->name, ent->id);
    ifp = fopen(ent->srcfn, "rb");
    if(ifp == NULL) {
      errprint("%zu: File not found: '%s'\n", i, ent->srcfn);
      return 1;
    }
    fseek(ifp, 0, SEEK_END);
    ent->rawsize = ftell(ifp);
    ent->rawdata = malloc(ent->rawsize);
    fseek(ifp, 0, SEEK_SET);
    fread(ent->rawdata, ent->rawsize, 1, ifp);
    fclose(ifp);
    if(ent->flags & FLAG_COMPRESSED) {
      // TODO
    }else{
      ent->size = ent->rawsize;
      ent->data = malloc(ent->size);
      memcpy(ent->data, ent->rawdata, ent->rawsize);
    }
    pack_size += ent->size;
  }
  fprintf(hdrfp, "\n#endif\n");
  fclose(hdrfp);

  size_t buildinfo_size = 0x20;
  size_t tbl_size = (max_id + 1) * sizeof(FsTblEnt);
  size_t meta_size = tbl_size + buildinfo_size;
  pack_size += meta_size;

  uint8_t *pack_data = malloc(pack_size);

  FsTblEnt *fstbl = (FsTblEnt*)pack_data;
  GameDef *game = NULL;

  *(fstbl++) = (FsTblEnt){
    .offset = endian32(game, s_fsent_count), // count
    .size = endian32(game, pack_size), // pack_size
    .flag = endian32(game, 0), // pad1
    .rawsize = endian32(game, 0), // pad2
  };

  size_t ptr = meta_size;
  for(size_t i = 0; i < s_fsent_count; i++) {
    FsEnt *ent = &s_fsents[i];
    memcpy(pack_data + ptr, ent->data, ent->size);
    fstbl[ent->id] = (FsTblEnt){
      .offset = endian32(game, ptr),
      .size = endian32(game, ent->size),
      .flag = endian32(game, ent->flags),
      .rawsize = endian32(game, ent->rawsize),
    };
    ptr += ent->size;
  }
  prv_write_timestamp((char*)pack_data + tbl_size);

  FILE *ofp = fopen(argv[1], "wb");
  fwrite(pack_data, pack_size, 1, ofp);
  fclose(ofp);
  return 0;
}
