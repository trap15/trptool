#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "wav.h"

#define A8_16(arr,idx) (arr[idx] | (arr[idx+1] << 8))
#define A8_32(arr,idx) (A8_16(arr,idx+0) | (A8_16(arr,idx+2) << 16))

static size_t prv_seek_to_chunk(FILE *fp, const char *chunk) {
  char name[5];
  // Skip to first chunk
  size_t skip = 0xC;
  fseek(fp, 0, SEEK_SET);
  do {
    fseek(fp, skip, SEEK_CUR);
    fread(name, 4, 1, fp);
    if(feof(fp))
      return 0;
    name[4] = '\0';
    uint8_t len[4];
    fread(len, 4, 1, fp);
    skip = A8_32(len, 0);
  } while(strcmp(chunk, name) != 0);
  return skip;
}

int wav_new(WavFile *wav, const char *fname) {
  FILE *fp = fopen(fname, "rb");
  *wav = (WavFile){0};

  uint8_t tmp[16];

  prv_seek_to_chunk(fp, "fmt ");
  fread(tmp, 16, 1, fp);
  if(A8_16(tmp, 0) != 1) {
    errprint("Bad WAV file: Data format must be PCM\n");
    return 0;
  }

  wav->channels = A8_16(tmp, 2);
  wav->samplerate = A8_32(tmp, 4);
  wav->samplebits = (A8_16(tmp, 14) + 7) & ~7;

  wav->datasize = prv_seek_to_chunk(fp, "data");
  wav->samples = wav->datasize * 8 / wav->samplebits / wav->channels;

  wav->data = calloc(wav->datasize, 1);
  fread(wav->data, wav->datasize, 1, fp);

  fclose(fp);

  return 1;
}
