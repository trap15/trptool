#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"

// in out

int cmd_pcx2bin(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_inpcx = argv[0];
  const char *arg_outbin = argv[1];

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_inpcx))
    return 1;

  FILE *fp = fopen(arg_outbin, "wb");
  for(size_t y = 0; y < pcx.h; y += 1) {
    uint8_t pix = 0;
    for(size_t x = 0; x < pcx.w; x += 1) {
      pix = pcx.data[x + (y * pcx.stride)];
      fwrite(&pix, 1, 1, fp);
    }
  }
  fclose(fp);

  return 0;
}
