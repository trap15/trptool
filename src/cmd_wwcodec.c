#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"

/* Actual codec action happens here */
static int ww_crypt_core(FILE *ifp, FILE *ofp, int dir) {
  int ret = 0;
  uint8_t last, buf;
  uint8_t byte_cnt;
  uint8_t c;

  byte_cnt = 0;
  for(;;) {
    if(fread(&c, 1, 1, ifp) != 1) {
      if(feof(ifp)) /* End of file reached */
        break;
      /* Otherwise it was an error! */
      ret = -1;
      break;
    }

    if(byte_cnt == 0) /* Reset last to 0xFF for each XMODEM sector */
      last = 0xFF;

    if(dir) { /* Encrypt */
      c ^= last;
      buf = c;
    }else{ /* Decrypt */
      buf = c;
      c ^= last;
    }

    last = buf;

    if(fwrite(&c, 1, 1, ofp) != 1) {
      /* Had to be an error! */
      ret = -1;
      break;
    }

    byte_cnt++;
    byte_cnt &= 0x7F; /* XMODEM sectors are every 0x80 bytes */
  }

  return ret;
}

static int ww_crypt(const char *infn, const char *outfn, int dir) {
  int ret = 0;
  FILE *ifp, *ofp;

  ifp = fopen(infn, "rb");
  if(ifp == NULL) {
    ret = -1;
    goto fail_noclose;
  }
  ofp = fopen(outfn, "wb");
  if(ofp == NULL) {
    ret = -1;
    goto fail_closeifp;
  }

  if(ww_crypt_core(ifp, ofp, dir)) {
    ret = -1;
    goto fail_closeall;
  }

fail_closeall:
  fclose(ofp);
fail_closeifp:
  fclose(ifp);
fail_noclose:
  return ret;
}

// format is something like u16, s8, s32
int cmd_wwcodec(int argc, const char *argv[]) {
  (void)argc;
  int encode;

  const char *arg_mode = argv[0];
  const char *arg_inbin = argv[1];
  const char *arg_outbin = argv[2];

  switch(arg_mode[0]) {
    case 'e': case 'E':
      encode = 1;
      break;
    case 'd': case 'D':
      encode = 0;
      break;
    default:
      errprint("Bad mode\n");
      return 1;
  }

  return ww_crypt(arg_inbin, arg_outbin, encode);
}
