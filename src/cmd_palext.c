#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>

#include "misc.h"
#include "format.h"

#define CFG_LINEBUF_SIZE 256
#define CFG_ROWMAP_SIZE 64

#define CFG_SYSTEM_COUNT 3

typedef struct {
  char filename[64];
  int row_first;
  int row_last;
  int writable;
} PaletteSource;

typedef struct {
  int source_idx;
  int source_row;
  int paldata_idx;
} PaletteLayoutRowMap;

typedef struct {
  char name[64];
  int source_count;
  PaletteSource sources[CFG_ROWMAP_SIZE];

  PaletteLayoutRowMap row_map[CFG_ROWMAP_SIZE];
} PaletteLayout;

typedef struct {
  char filename[64];
  int row;
  int writable;
  uint16_t data[CFG_SYSTEM_COUNT][16];
} PaletteData;

static PaletteData s_paldata[1024];
static int s_paldata_count;
static int s_writable_count;

static PaletteLayout s_layouts[256];
static int s_layout_count;

static char *read_cfg(const char *path) {
  FILE *fp = fopen(path, "rb");
  fseek(fp, 0, SEEK_END);
  size_t cfg_bufsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  char *cfg = malloc(cfg_bufsize+1);
  cfg_bufsize = 0;

  while(!feof(fp)) {
    char linebuf[CFG_LINEBUF_SIZE];
    fgets(linebuf, CFG_LINEBUF_SIZE, fp);
    for(size_t i = 0; i < CFG_LINEBUF_SIZE && linebuf[i]; i++) {
      char c = linebuf[i];
      if(c == '#') break; // Comment
      if(!isspace(c)) {
        cfg[cfg_bufsize++] = c;
      }
    }
  }
  cfg[cfg_bufsize] = 0;
  fclose(fp);

  return cfg;
}

static const char *s_sys_extension[CFG_SYSTEM_COUNT] = {
  "wsc", "sct", "port",
};

static int read_paldata(GameDef *game, const char *arg_pathin, const char *filename, int row, int writable) {
  (void)game;
  for(int i = 0; i < s_paldata_count; i++) {
    if(strcmp(filename, s_paldata[i].filename) != 0) {
      continue;
    }

    // Row already in paldata database, return its index.
    if(row == s_paldata[i].row) {
      return i;
    }
  }
  PaletteData *dat = &s_paldata[s_paldata_count++];
  strcpy(dat->filename, filename);
  dat->row = row;
  dat->writable = writable;

  // Need to add new paldata entry.
  char fullname[512];
  for(int sys = 0; sys < CFG_SYSTEM_COUNT; sys++) {
    sprintf(fullname, "%s/%s.%s.pcx", arg_pathin, filename, s_sys_extension[sys]);

    FILE *fp = fopen(fullname, "rb");
    if(fp == NULL) {
      // default to no-system if system variant is not present
      sprintf(fullname, "%s/%s.pcx", arg_pathin, filename);
      fp = fopen(fullname, "rb");
    }
    assert(fp != NULL);

    fseek(fp, -0x300 + (row * 0x30), SEEK_END);
    uint8_t col[3];
    for(int i = 0; i < 0x10; i++) {
      fread(col, 3, 1, fp);
      col[0] >>= 4;
      col[1] >>= 4;
      col[2] >>= 4;
      dat->data[sys][i] = (col[0] << 8) | (col[1] << 4) | (col[2] << 0);
    }
    fclose(fp);
  }
  return s_paldata_count - 1;
}

static void parse_cfg(GameDef *game, char *cfg, const char *arg_pathin) {
  char *next;
  while(*cfg) {
    PaletteLayout *cur_lyt = &s_layouts[s_layout_count];
    memset(cur_lyt, 0, sizeof(PaletteLayout));

    // Extract name
    next = strchr(cfg, '{');
    if(next == NULL) {
      break;
    }
    *next = '\0'; next++;
    strcpy(cur_lyt->name, cfg);
    cfg = next;

    // Extract rows
    while(*cfg != '}') {
      PaletteSource *cur_src = &cur_lyt->sources[cur_lyt->source_count++];
      memset(cur_src, 0, sizeof(PaletteSource));

      // Extract filename
      next = strchr(cfg, ':');
      assert(next != NULL);
      *next = '\0'; next++;
      if(cfg[0] == '+') {
        cur_src->writable = 1 + ((s_writable_count++) * 17);
        cfg++;
      }
      strcpy(cur_src->filename, cfg);
      cfg = next;

      // Extract row(s)
      next = strchr(cfg, ',');
      if(next == NULL) {
        next = strchr(cfg, '}');
        assert(next != NULL);
        *next = '\0';
        next = NULL;
      }else{
        *next = '\0'; next++;
      }
      char *row_endptr;
      cur_src->row_first = strtol(cfg, &row_endptr, 0);
      if(*row_endptr == '\0') {
        cur_src->row_last = cur_src->row_first;
      }else{
        assert(row_endptr[0] == '.');
        assert(row_endptr[1] == '.');
        cur_src->row_last = strtol(row_endptr + 2, &cfg, 0);
      }

      if(next == NULL) {
        break;
      }else{
        cfg = next;
      }
    }
    cfg++;
    s_layout_count++;
  }

  for(int i = 0; i < s_layout_count; i++) {
    PaletteLayout *lyt = &s_layouts[i];
    int row = 0;
    for(int l = 0; l < lyt->source_count; l++) {
      PaletteSource *src = &lyt->sources[l];
      for(int r = src->row_first; r <= src->row_last; r++) {
        assert(row < CFG_ROWMAP_SIZE);
        lyt->row_map[row].source_idx = l;
        lyt->row_map[row].source_row = r;
        lyt->row_map[row].paldata_idx = read_paldata(game, arg_pathin, src->filename, r, src->writable);
        row++;
      }
    }
    while(row < CFG_ROWMAP_SIZE) {
      lyt->row_map[row].source_idx = -1;
      lyt->row_map[row].source_row = -1;
      lyt->row_map[row].paldata_idx = -1;
      row++;
    }
  }
}

#if 0
static void debug_cfg(void) {
  printf("%d layouts parsed\n", s_layout_count);
  for(int i = 0; i < s_layout_count; i++) {
    PaletteLayout *lyt = &s_layouts[i];
    printf("[%2d] = { /* %s */\n", i, lyt->name);
    for(int l = 0; l < CFG_ROWMAP_SIZE; l++) {
      if(lyt->row_map[l].source_idx == -1) break;
      printf("  [%02Xh] = %s:%d (%d)\n", l, lyt->sources[lyt->row_map[l].source_idx].filename, lyt->row_map[l].source_row, lyt->row_map[l].paldata_idx);
    }
    printf("}\n");
  }
}
#endif

// fmt inmap.cfg inpath/ out.c
int cmd_palext(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_fmt = argv[0];
  const char *arg_cfgin = argv[1];
  const char *arg_pathin = argv[2];
  const char *arg_outfile = argv[3];
  const char *arg_type = argv[4];

  GameDef *game = fmt_get(arg_fmt);
  if(game == NULL) {
    errprint("bad fmt %s\n", arg_fmt);
    return 1;
  }
  char *cfg = read_cfg(arg_cfgin);
  parse_cfg(game, cfg, arg_pathin);
  //debug_cfg();

  FILE *fp = fopen(arg_outfile, "wb");
  if(arg_type[0] == 'c') {
    fprintf(fp, "#include \"top.h\"\n");
    fprintf(fp, "////////////////\n// Palmap\n////////////////\n");
    for(int i = 0; i < s_layout_count; i++) {
      PaletteLayout *lyt = &s_layouts[i];
      fprintf(fp, "const WORD g_palmap_%s", lyt->name);
      if(lyt->row_map[0].source_idx == -1) {
        fprintf(fp, "[EMPTY_CONST_ARRAY_SIZE];\n");
      }else{
        fprintf(fp, "[] = { ");
        for(int l = 0; l < CFG_ROWMAP_SIZE; l++) {
          if(lyt->row_map[l].source_idx == -1) {
            break;
          }
          fprintf(fp, "0x%04X,", lyt->row_map[l].paldata_idx * 17);
        }
        fprintf(fp, " };\n");
      }
    }
#define PALDATA_IN_RSRC 1
#if !PALDATA_IN_RSRC
    fprintf(fp, "////////////////\n// Paldata\n////////////////\n");
    fprintf(fp, "WORD g_paldata_raw[] = {\n");
    fprintf(fp, "0x%X,\n", s_paldata_count * 17);
    fprintf(fp, "0x%X,\n", s_writable_count);
    for(int sys = 0; sys < CFG_SYSTEM_COUNT; sys++) {
      fprintf(fp, "// %s\n", s_sys_paldata[sys]);
      for(int i = 0; i < s_paldata_count; i++) {
        fprintf(fp, "  // %d (%s:%d)\n  ", i, s_paldata[i].filename, s_paldata[i].row);
        for(int l = 0; l < 16; l++) {
          fprintf(fp, "0x%03X,", s_paldata[i].data[sys][l]);
        }
        fprintf(fp, "%d,", s_paldata[i].writable);
        fprintf(fp, "\n");
      }
    }
    fprintf(fp, "};\n");
#endif
  }else if(arg_type[0] == 'b') {
    uint8_t buf[4];
    uint16_t paldata_size = s_paldata_count * 17;
    buf[0] = paldata_size & 0xFF;
    buf[1] = paldata_size >> 8;
    fwrite(buf, 2, 1, fp);
    buf[0] = s_writable_count & 0xFF;
    buf[1] = s_writable_count >> 8;
    fwrite(buf, 2, 1, fp);
    for(int sys = 0; sys < CFG_SYSTEM_COUNT; sys++) {
      for(int i = 0; i < s_paldata_count; i++) {
        fwrite(s_paldata[i].data[sys], 16, 2, fp);
        buf[0] = s_paldata[i].writable & 0xFF;
        buf[1] = s_paldata[i].writable >> 8;
        fwrite(buf, 2, 1, fp);
      }
    }
  }
  fclose(fp);

  return 0;
}
