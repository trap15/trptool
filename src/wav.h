#ifndef WAV_H_
#define WAV_H_

typedef struct {
  int channels;
  int samplerate;
  int samplebits;
  size_t samples;
  size_t datasize;
  void *data;
} WavFile;

int wav_new(WavFile *wav, const char *fname);

#endif
