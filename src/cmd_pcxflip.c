#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// in out [x|y]

int cmd_pcxflip(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin = argv[0];
  const char *arg_pcxout = argv[1];
  const char *arg_fliptype = argv[2];

  int fliptype = 0;
  while(*arg_fliptype) {
    switch(*arg_fliptype) {
      case 'x': case 'X': fliptype ^= 1; break;
      case 'y': case 'Y': fliptype ^= 2; break;
    }
    arg_fliptype++;
  }

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;

  PcxFile opcx;
  opcx.bpp = pcx.bpp;
  opcx.w = pcx.w;
  opcx.h = pcx.h;
  memcpy(opcx.pal16, pcx.pal16, sizeof(pcx.pal16));
  opcx.stride = opcx.w * opcx.bpp / 8;
  opcx.data = malloc(opcx.h * opcx.stride);
  opcx.pal = pcx.pal;

  for(size_t y = 0; y < opcx.h; y++) {
    size_t oy = y;
    if(fliptype & 2) {
      oy = pcx.h - y - 1;
    }
    for(size_t x = 0; x < opcx.w; x++) {
      size_t ox = x;
      if(fliptype & 1) {
        ox = pcx.w - x - 1;
      }
      opcx.data[(y*opcx.stride)+x] = pcx.data[(oy*pcx.stride)+ox];
    }
  }

  pcx_save(&opcx, arg_pcxout);

  return 0;
}
