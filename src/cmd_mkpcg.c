#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"
#include "pcg.h"

static int prv_has_hash(uint32_t *hashes, size_t max, uint32_t hash, uint16_t *til) {
  for(size_t i = 0; i < max; i++) {
    if(hashes[i] != hash)
      continue;
    *til = i;
    return 1;
  }
  return 0;
}

// rotmode:
//   lowercase affects full image, uppercase affects per-tile
//   x = xflip, y = yflip, r = 90deg rotate
//   using XOR, so xX will xflip tile order, but not tile content
//   s = small MAP
//   p = output palette

int cmd_mkpcg(int argc, const char *argv[]) {
  int xflip = 0, yflip = 0, rot = 0;
  int xflip_t = 0, yflip_t = 0, rot_t = 0;
  int small_map = 0, out_pal = 0, zoomspr = 0;
  const char *arg_fmt = argv[0];
  const char *arg_rotmode = argv[1];
  const char *arg_pcxin = argv[2];
  const char *arg_outpcg = argv[3];
  const char *arg_outmap = argc > 4 ? argv[4] : NULL;
  const char *arg_outpal = NULL;

  for(int i = 0; arg_rotmode[i] != '\0'; i++) {
    switch(arg_rotmode[i]) {
      case 's': small_map ^= 1; break;
      case 'r': rot ^= 1; rot_t ^= 1; break;
      case 'x': xflip ^= 1; xflip_t ^= 1; break;
      case 'y': yflip ^= 1; yflip_t ^= 1; break;
      case 'R': rot_t ^= 1; break;
      case 'X': xflip_t ^= 1; break;
      case 'Y': yflip_t ^= 1; break;
      case 'p': out_pal ^= 1; break;
      case 'z': zoomspr ^= 1; break;
    }
  }

  if(out_pal) {
    arg_outpal = arg_outmap;
    arg_outmap = argc > 5 ? argv[5] : NULL;
  }

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;

  GameDef *game = fmt_get(arg_fmt);
  if(game == NULL) {
    errprint("bad fmt %s\n", arg_fmt);
    return 1;
  }

  size_t tiles_w = pcx.w / game->hwdef->pcg_w;
  size_t tiles_h = pcx.h / game->hwdef->pcg_h;

  size_t tile_count = tiles_w * tiles_h;
  size_t tile_size = pcg_tile_size(game);
  uint8_t *tile_data = malloc(tile_count * tile_size * 2);
  memset(tile_data, 0xFF, tile_size);
  uint32_t cull_tile_hash = hash32(tile_data, tile_size);
  memset(tile_data, 0, tile_count * tile_size * 2);
  uint32_t empty_tile_hash = hash32(tile_data, tile_size);

  uint16_t *map = calloc(4, tile_count*2);
  uint32_t *hashes = calloc(4, tile_count*2);

  uint8_t *hash_ptr = calloc(1, tile_size);
  uint8_t *ptr = tile_data;
  size_t data_idx = 0, pos_idx = 0;
  for(int flip = 0; flip < (zoomspr ? 2 : 1); flip++) {
    if(flip) {
      xflip ^= 1;
      xflip_t ^= 1;
    }
    for(size_t y = 0; y < tiles_h; y++) {
      for(size_t x = 0; x < tiles_w; x++) {
        size_t px = x, py = y;

        flip_perform(&px,&py, tiles_w,tiles_h, xflip, yflip, rot);

        size_t rx = px * game->hwdef->pcg_w;
        size_t ry = py * game->hwdef->pcg_h;

        uint32_t pal;
        pal = pcg_build_tile(ptr, game, &pcx, rx, ry, xflip_t, yflip_t, rot_t);

        uint16_t attr, til;
        // Handle map stuff
        if(arg_outmap != NULL) {
          uint32_t hash = hash32(ptr, tile_size);
          if(zoomspr && hash == cull_tile_hash && pal == 15) {
            continue;
          }
          if(hash == empty_tile_hash) {
            attr = 0;
            til = game->hwdef->map_emptyval;
            goto _emit;
          }
          attr = 0;
          if(prv_has_hash(hashes,data_idx, hash, &til))
            goto _emit;

          pcg_build_tile(hash_ptr, game, &pcx, rx, ry, xflip_t ^ 1, yflip_t ^ 0, rot_t);
          attr = (game->hwdef->map_rotate > 0) ? game->hwdef->map_yflip_bit : game->hwdef->map_xflip_bit;
          if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
            goto _emit;

          pcg_build_tile(hash_ptr, game, &pcx, rx, ry, xflip_t ^ 0, yflip_t ^ 1, rot_t);
          attr = (game->hwdef->map_rotate > 0) ? game->hwdef->map_xflip_bit : game->hwdef->map_yflip_bit;
          if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
            goto _emit;

          pcg_build_tile(hash_ptr, game, &pcx, rx, ry, xflip_t ^ 1, yflip_t ^ 1, rot_t);
          attr = game->hwdef->map_xflip_bit | game->hwdef->map_yflip_bit;
          if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
            goto _emit;

          hashes[data_idx] = hash;
        }
        ptr += tile_size;

        attr = 0;
        til = data_idx;
        data_idx++;
_emit:
        attr ^= game->hwdef->map_neutral_bit;
        if(small_map) {
          til |= attr;
        }else{
          attr |= pal << game->hwdef->map_pal_bit;
        }
        map[pos_idx] = endian16(game, til);
        pos_idx++;
        if(!small_map) {
          map[pos_idx] = endian16(game, attr);
          pos_idx++;
        }
      }
    }
  }

  if(arg_outmap != NULL) {
    FILE *fp = fopen(arg_outmap, "wb");
    fwrite(map, pos_idx, 2, fp);
    fclose(fp);
    dbgprint("Tile count: 0x%zX (%zu%% ratio)\n", data_idx, data_idx * 100 / pos_idx);
  }

  if(arg_outpal != NULL) {
    FILE *fp = fopen(arg_outpal, "wb");
    for(unsigned int i = 0; i < game->hwdef->pal_size; i++) {
      uint8_t r = pcx.pal[i*3 + 0];
      uint8_t g = pcx.pal[i*3 + 1];
      uint8_t b = pcx.pal[i*3 + 2];
      uint32_t color = 0;
      color |= (r >> (8 - game->hwdef->pal_r_size)) << game->hwdef->pal_r_shift;
      color |= (g >> (8 - game->hwdef->pal_g_size)) << game->hwdef->pal_g_shift;
      color |= (b >> (8 - game->hwdef->pal_b_size)) << game->hwdef->pal_b_shift;
      if(game->hwdef->pal_depth <= 8) {
        uint8_t val = color;
        fwrite(&val, 1, 1, fp);
      }else if(game->hwdef->pal_depth <= 16) {
        uint16_t val = endian16(game, color);
        fwrite(&val, 2, 1, fp);
      }else if(game->hwdef->pal_depth <= 32) {
        uint32_t val = endian32(game, color);
        fwrite(&val, 4, 1, fp);
      }
    }

    fclose(fp);
  }

  FILE *fp = fopen(arg_outpcg, "wb");
  fwrite(tile_data, data_idx, tile_size, fp);
  fclose(fp);

  return 0;
}
