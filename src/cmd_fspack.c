#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "misc.h"
#include "lze.h"

// TODO: Could probably make this more platform agnostic
enum {
  FmtType_ROM,
  FmtType_WWSft,
  FmtType_Freya,
  FmtType_PC,
  FmtType_NGP,
  FmtType_MD,

  FmtTypeCount,
};

static int s_fmt_type;
static const char *s_footnote;
static const char **s_builds;
static size_t s_build_count;

#define MAX_FSENT 512
#define MAX_DATA_LENGTH 256
#define MAX_LINE_LENGTH 256

typedef struct {
  uint16_t id;
  char name[MAX_DATA_LENGTH];
  char srcfn[MAX_DATA_LENGTH];
  char build_type[MAX_DATA_LENGTH];
  int bank;
  int align;
  int compressed;

  size_t size;
  void *data;
} FsEnt;

typedef struct __attribute__((packed)) {
  uint8_t ptr[4];
  uint8_t size[4];
} FsTblEnt;

typedef struct {
  size_t size;
  size_t pos;
  uint8_t *data;
} BankData;

static size_t s_rombanks = 8;
static FsEnt s_fsents[MAX_FSENT];
static size_t s_fsent_count;

static int prv_str2bank(const char *str) {
  if(strcmp(str, "FIX") == 0) {
    return 0;
  }else{ // TODO: idk
    return 1 + strtol(str, NULL, 0);
  }
}

static void prv_check_id(size_t max_id) {
  for(size_t i = 0; i < s_fsent_count; i++) {
    if(max_id == s_fsents[i].id) {
      errprint("Duplicate ID %zu\n", max_id);
    }
  }
}

static int prv_handle_section(char *key, size_t *max_id) {
  char *val;
  int was_file = 0;
  val = strchr(key, '=');
  if(val == NULL)
    return 0;
  *(val++) = '\0';

  if(strcmp(key, "ID") == 0) {
    *max_id = strtol(val, NULL, 0);
    prv_check_id(*max_id);
  }else if(strcmp(key, "NAME") == 0) {
    strncpy(s_fsents[s_fsent_count].name, val, MAX_DATA_LENGTH);
    was_file = 1;
  }else if(strcmp(key, "SOURCE") == 0) {
    strncpy(s_fsents[s_fsent_count].srcfn, val, MAX_DATA_LENGTH);
    was_file = 1;
  }else if(strcmp(key, "BANK") == 0) {
    s_fsents[s_fsent_count].bank = prv_str2bank(val);
    was_file = 1;
  }else if(strcmp(key, "ALIGNED") == 0) {
    s_fsents[s_fsent_count].align = strtol(val, NULL, 0) ? 4 : 0;
    was_file = 1;
  }else if(strcmp(key, "ALIGNMENT") == 0) {
    s_fsents[s_fsent_count].align = strtol(val, NULL, 0);
    was_file = 1;
  }else if(strcmp(key, "BUILD") == 0) {
    strncpy(s_fsents[s_fsent_count].build_type, val, MAX_DATA_LENGTH);
    was_file = 1;
  }else if(strcmp(key, "COMPRESS") == 0) {
    s_fsents[s_fsent_count].compressed = strtol(val, NULL, 0);
    was_file = 1;
  }
  return was_file;
}

static int prv_handle_line(char *str, size_t *max_id) {
  char *tok;
  int was_file = 0;
  do {
    tok = strtok(str, ";");
    str = NULL;
    if(tok == NULL)
      break;
    if(tok[0] == '\0')
      break;
    // Strip trailing spaces
    char *ptr = tok + strlen(tok);
    while(*ptr == ' ' && ptr != tok) {
      *ptr = '\0';
      ptr--;
    }
    // Strip prefaced spaces
    while(*tok == ' ') tok++;
    if(prv_handle_section(tok, max_id))
      was_file = 1;
  } while(tok != NULL);
  return was_file;
}

static BankData prv_bankdata(size_t size) {
  BankData bank;
  bank.size = size;
  bank.pos = 0;
  bank.data = calloc(bank.size, 1);
  return bank;
}

static void prv_writebank(BankData *bank, FILE *ofp) {
  if(bank->size == 0)
    return;
  fwrite(bank->data, bank->size, 1, ofp);
}

static size_t prv_export_bank_info(char *buf, BankData *bnk) {
  buf[0x00] = bnk->pos >> 0;
  buf[0x01] = bnk->pos >> 8;
  buf[0x02] = bnk->pos >> 16;
  buf[0x03] = bnk->pos >> 24;
  buf[0x04] = bnk->size >> 0;
  buf[0x05] = bnk->size >> 8;
  buf[0x06] = bnk->size >> 16;
  buf[0x07] = bnk->size >> 24;
  return 8;
}

static size_t prv_footnote_size(void) {
  size_t len = strlen(s_footnote);
  return (len + 0xF) & ~0xF;
}

static size_t prv_buildinfo_size(size_t fixbank_count, size_t exbank_count) {
  size_t size = 0;
  fixbank_count = 1;

  size += 8 + 8; // raw time + padding
  size += (fixbank_count * 0x8);
  size += 0x10 * 3; // VER., TIME, DATE
  size += (fixbank_count * 0x10) + (exbank_count * 0x10);
  size += prv_footnote_size(); // footnote
  size += 0x10; // tail ptr space
  return size;
}

static size_t prv_make_buildinfo(char *str, size_t max, BankData *fix, BankData *ex, size_t ex_cnt) {
  time_t t = time(NULL);
  struct tm ti;
  localtime_r(&t, &ti);
  memset(str, 0, max);
  size_t idx = 0;
  str[idx+0x00] = (ti.tm_year - 100);
  str[idx+0x01] = ti.tm_mon;
  str[idx+0x02] = ti.tm_mday - 1;
  str[idx+0x03] = ti.tm_hour;
  str[idx+0x04] = ti.tm_min;
  str[idx+0x05] = ti.tm_sec;
  str[idx+0x06] = 0;
  str[idx+0x07] = 0;
  idx += 8;
  // padding
  idx += 8;
  idx += prv_export_bank_info(str+idx, fix);

  uint32_t ver = (ti.tm_year - 110);
  ver = ti.tm_yday + (ver * 366);
  ver = ti.tm_hour + (ver * 24);
  ver = ti.tm_min + (ver * 60);
  sprintf(str+idx, "VER.%06X", ver); idx += 0x10;
  strftime(str+idx, 0x10, "TIME:%H:%M:%S", &ti); idx += 0x10;
  strftime(str+idx, 0x10, "DATE:%Y.%m.%d", &ti); idx += 0x10;
//  sprintf(str+idx, "PRG:%05zX/%05zX", prg->pos, prg->size); idx += 0x10;
  sprintf(str+idx, "FIX:%05zX/%05zX", fix->pos, fix->size); idx += 0x10;
  for(size_t i = 0; i < ex_cnt; i++) {
    sprintf(str+idx, "B%02zd:%05zX/%05zX", i, ex[i].pos, ex[i].size); idx += 0x10;
  }
  sprintf(str+idx, "%s", s_footnote); idx += prv_footnote_size();

  str[idx++] = '\0';
  return idx;
}

static int prv_check_build_ok(const char *type) {
  if(type[0] == '\0') {
    return 1;
  }
  int build_allowed = 0;
  char *tok;
  char *nstr = strdup(type);
  char *str = nstr;
  do {
    tok = strtok(str, ",");
    str = NULL;
    if(tok == NULL)
      break;
    if(tok[0] == '\0')
      break;
    int value = 1;
    if(tok[0] == '!') {
      value = 0;
      tok++;
    }
    if(strcmp(tok, "all") == 0) {
      build_allowed = value;
    }else if(strcmp(tok, "none") == 0) {
      build_allowed = !value;
    }else{
      for(size_t i = 0; i < s_build_count; i++) {
        if(strcmp(tok, s_builds[i]) == 0) {
          build_allowed = value;
        }
      }
    }
  } while(tok != NULL);
  free(nstr);
  return build_allowed;
}

static void prv_emit_header(void) {
  switch(s_fmt_type) {
    case FmtType_NGP: // AS type
      printf("; rsrcid.h\n"
             "\n"
             "; fspack build rules\n");
      for(size_t i = 0; i < s_build_count; i++) {
        printf("RSRCBUILD_%s = 1\n", s_builds[i]);
      }
      printf("\n"
             "; Technically valid, gives program\n"
             "RSRCID_INVALID = 0\n"
             "\n");
      break;
    default: // C type
      printf("#ifndef RSRCID_H_\n"
             "#define RSRCID_H_\n"
             "\n"
             "// fspack build rules\n");
      for(size_t i = 0; i < s_build_count; i++) {
        printf("#define RSRCBUILD_%s 1\n", s_builds[i]);
      }
      printf("\n"
             "// Technically valid, gives program\n"
             "#define RSRCID_INVALID (0)\n"
             "\n");
      break;
  }
}

static void prv_emit_rsrcid(FsEnt *ent) {
  switch(s_fmt_type) {
    case FmtType_NGP: // AS type
      printf("RSRCID_%s = 0%03Xh\n", ent->name, ent->id);
      break;
    default: // C type
      printf("#define RSRCID_%s (0x%03X)\n", ent->name, ent->id);
      break;
  }
}

static void prv_emit_filesize(size_t raw_size, size_t entsize, int compressed) {
  switch(s_fmt_type) {
    case FmtType_NGP: // AS type
      printf(";");
      break;
    default: // C type
      printf("//");
      break;
  }
  printf(" Size: 0x%05zX", raw_size);
  if(compressed) {
    double pct = entsize * 10000 / raw_size;
    printf(" | Compressed: 0x%05zX (%.2f%%)", entsize, pct / 100);
  }
  printf("\n");
}

uint32_t vendian32(int doflip, uint32_t value) {
  if(doflip) {
    value = ((value & 0xFF000000) >> 24) |
            ((value & 0x00FF0000) >>  8) |
            ((value & 0x0000FF00) <<  8) |
            ((value & 0x000000FF) << 24);
  }
  return value;
}

uint16_t vendian16(int doflip, uint16_t value) {
  if(doflip) {
    value = ((value & 0xFF00) >> 8) |
            ((value & 0x00FF) << 8);
  }
  return value;
}

int cmd_fspack(int argc, char *argv[]) {
  s_fmt_type = FmtType_ROM;

  int endian = 0;
  int dryrun = 0;
  if(strcmp(argv[1], "^") == 0) {
    dryrun = 1;
  }

#define MAX_BUILD_SPECS 16
  s_builds = malloc(sizeof(const char *) * MAX_BUILD_SPECS);
  char *arg_fmt_type = strdup(argv[2]);
  char *start = arg_fmt_type;
  for(char *ptr = start; *ptr; ptr++) {
    if(*ptr == ',') {
      *ptr = '\0';
      s_builds[s_build_count++] = start;
      start = ptr + 1;
    }
  }
  s_builds[s_build_count++] = start;

  if(strcmp(arg_fmt_type, "wwsft") == 0)
    s_fmt_type = FmtType_WWSft;
  else if(strcmp(arg_fmt_type, "freya") == 0)
    s_fmt_type = FmtType_Freya;
  else if(strcmp(arg_fmt_type, "rom") == 0)
    s_fmt_type = FmtType_ROM;
  else if(strcmp(arg_fmt_type, "pc") == 0)
    s_fmt_type = FmtType_PC;
  else if(strcmp(arg_fmt_type, "ngp") == 0)
    s_fmt_type = FmtType_NGP;
  else if(strcmp(arg_fmt_type, "md") == 0)
    s_fmt_type = FmtType_MD;

  switch(s_fmt_type) {
    case FmtType_MD:
      endian = 1;
      break;
    default:
      endian = 0;
      break;
  }

  s_rombanks = strtol(argv[3], NULL, 0);

  if(argc > 4) {
    s_footnote = argv[4];
  }else{
    s_footnote = "";
  }

  FILE *ifp = fopen(argv[0], "r");
  if(ifp == NULL) {
    errprint("File not found: '%s'\n", argv[0]);
    return 1;
  }

  char line[MAX_LINE_LENGTH];

  size_t max_id = 0;
  memset(s_fsents, 0, sizeof(s_fsents));
  s_fsent_count = 0;
  while(!feof(ifp)) {
    line[0] = '\0';
    fgets(line, MAX_LINE_LENGTH, ifp);
    char *s = strchr(line, '#');
    if(s != NULL) *s = '\0';
    s = strchr(line, '\r');
    if(s != NULL) *s = '\0';
    s = strchr(line, '\n');
    if(s != NULL) *s = '\0';

    if(line[0] == '\0')
      continue;
    if(prv_handle_line(line, &max_id)) {
      s_fsents[s_fsent_count].id = max_id;
      max_id++;
      s_fsent_count++;
      if(s_fsent_count >= MAX_FSENT) {
        errprint("Too many FS entries! Max is %d!\n", MAX_FSENT);
        return 1;
      }
      s_fsents[s_fsent_count].id = max_id;
    }
  }

  fclose(ifp);

  prv_emit_header();

  for(size_t i = 0; i < s_fsent_count; i++) {
    FsEnt *ent = &s_fsents[i];
    if(ent->id > max_id)
      max_id = ent->id;

    if(!dryrun && !prv_check_build_ok(ent->build_type)) {
      // Dump rsrcid
      prv_emit_rsrcid(ent);
      continue;
    }
    ifp = fopen(ent->srcfn, "rb");
    if(!dryrun && ifp == NULL) {
      errprint("%zu: File not found: '%s'\n", i, ent->srcfn);
      return 1;
    }
    if(ifp) {
      fseek(ifp, 0, SEEK_END);
      ent->size = ftell(ifp);
      ent->data = malloc(ent->size);
      size_t raw_size = ent->size;
      fseek(ifp, 0, SEEK_SET);
      fread(ent->data, ent->size, 1, ifp);
      if(ent->compressed) {
        void *newdata = malloc(ent->size * 2);
        ent->size = lze_encode(newdata, ent->data, ent->size);
        free(ent->data);
        ent->data = newdata;
      }
      fclose(ifp);
      prv_emit_filesize(raw_size, ent->size, ent->compressed);
    }
    // Dump rsrcid
    prv_emit_rsrcid(ent);
  }
  printf("\n#endif\n");
  if(dryrun) {
    return 0;
  }

  switch(s_fmt_type) {
    case FmtType_ROM:
      break;
    case FmtType_WWSft:
      // WWSft has a fixed layout
      s_rombanks = 8;
      break;
    case FmtType_Freya:
      // Freya is pretty special
      s_rombanks = 8;
      break;
  }

  size_t exbank_count, fixbank_count;

  switch(s_fmt_type) {
    case FmtType_ROM:
    case FmtType_WWSft:
    case FmtType_Freya:
      exbank_count = s_rombanks;

      fixbank_count = exbank_count;
      if(fixbank_count > 12) fixbank_count = 12;
      exbank_count -= fixbank_count;
      break;
    case FmtType_NGP:
    case FmtType_MD:
    case FmtType_PC:
      exbank_count = 0;
      fixbank_count = s_rombanks;
      break;
  }
  size_t real_fixsize = fixbank_count << 16;

  BankData fix = prv_bankdata(real_fixsize);
  switch(s_fmt_type) {
    case FmtType_ROM:
      fix.size -= 0x20; // wshead space
      break;
    case FmtType_PC:
      fix.size -= 0x20; // wshead space
      break;
    case FmtType_WWSft:
      fix.size -= 0x20; // wshead space
      // Reserved area for Freya
      fix.size -= 0x10000;
      break;
    case FmtType_Freya:
      // Freya is pretty special
      fix.size -= 0x10000;
      break;
    case FmtType_NGP:
    case FmtType_MD:
      break;
  }

  BankData *exbanks = exbank_count ? calloc(sizeof(BankData), exbank_count) : NULL;
  for(size_t i = 0; i < exbank_count; i++) {
    exbanks[i] = prv_bankdata(0x10000);
  }

  int meta_prefix;
  switch(s_fmt_type) {
    case FmtType_ROM:
    case FmtType_WWSft:
    case FmtType_PC:
    case FmtType_NGP:
    case FmtType_MD:
      meta_prefix = 0;
      break;
    case FmtType_Freya:
      meta_prefix = 1;
      break;
  }

  size_t saved_fix_size = fix.size;
  switch(s_fmt_type) {
    case FmtType_ROM:
    case FmtType_PC:
    case FmtType_WWSft:
    case FmtType_Freya:
    case FmtType_MD:
      break;
    case FmtType_NGP:
      fix.size -= 0x4000; // system reserved space
      break;
  }


  size_t tail_ptr_size = 16;
  size_t buildinfo_size = prv_buildinfo_size(fixbank_count, exbank_count) - tail_ptr_size;
  size_t tbl_size = (max_id + 1) * sizeof(FsTblEnt);
  size_t meta_size = buildinfo_size + tail_ptr_size + tbl_size;

  size_t tbl_offset;
  if(meta_prefix) {
    tbl_offset = 0;
    fix.pos += meta_size;
  }else{
    fix.size -= meta_size;
    tbl_offset = fix.size;
  }
  size_t info_offset = tbl_offset + tbl_size;
  size_t tail_offset = info_offset + buildinfo_size;

  FsTblEnt *fstbl = (FsTblEnt*)(fix.data + tbl_offset);

  for(size_t i = 0; i < s_fsent_count; i++) {
    FsEnt *ent = &s_fsents[i];
    if(!prv_check_build_ok(ent->build_type))
      continue;
    BankData *bank;
    switch(ent->bank) {
      case  0: bank = &fix; break;
      default: bank = &exbanks[ent->bank-1]; break;
    }
    if(bank == NULL) {
      errprint("bank %d invalid\n", ent->bank);
    }
    uint32_t align_val = (1 << (ent->align ? ent->align : 2)) - 1;
    bank->pos = (bank->pos + align_val) & ~align_val;
    if(bank->size - bank->pos < ent->size) {
      errprint("Entry %zu cannot fit in bank!\n", i);
    }
    memcpy(bank->data + bank->pos, ent->data, ent->size);
    uint32_t ptr = ((bank->pos >> 4) << 16) | (bank->pos & 0xF);
    uint8_t reqbank = 0;
    switch(s_fmt_type) {
      case FmtType_ROM:
      case FmtType_WWSft:
        switch(ent->bank) {
          case 0:
            ptr += (0x10000 - (real_fixsize >> 4)) << 16; // ROM2
            break;
          default:
            ptr += 0x3000 << 16; // ROM1
            reqbank = 0x100 - s_rombanks + ent->bank-2;
            break;
        }
        break;
      case FmtType_PC:
      case FmtType_MD:
        ptr = bank->pos;
        break;
      case FmtType_NGP:
        ptr = bank->pos + 0x200000;
        break;
      case FmtType_Freya:
        // Freya builds need to do their own segment relocation
        break;
    }
    // Don't care, for now.
    (void)reqbank;

    uint32_t ptr_save = vendian32(endian, ptr);
    uint32_t size_save = vendian32(endian, ent->size);
    fstbl[ent->id] = (FsTblEnt){
      .ptr = { ptr_save, ptr_save >> 8, ptr_save >> 16, ptr_save >> 24 },
      .size = { size_save, size_save >> 8, size_save >> 16, size_save >> 24 },
    };
    bank->pos += ent->size;
  }
  char *buildinfo = calloc(buildinfo_size, 1);
  prv_make_buildinfo(buildinfo, buildinfo_size, &fix, exbanks, exbank_count);
  fix.size = saved_fix_size;

  uint32_t offset_base = 0;
  uint32_t offset_mask = 0xFFFFFFFF;
  switch(s_fmt_type) {
    case FmtType_PC:
    case FmtType_MD:
      break;
    case FmtType_NGP:
      offset_base = 0x200000;
      break;
    case FmtType_ROM:
      offset_base = 0xF000 << 16;
      offset_mask = 0xFFFF;
      break;
    case FmtType_WWSft:
      // Reserved area for Freya
      offset_base = 0xE000 << 16;
      offset_mask = 0xFFFF;
      break;
    case FmtType_Freya:
      // Freya is pretty special
      offset_base = 0x0000 << 16;
      offset_mask = 0xFFFF;
      break;
  }
  memcpy((char *)fix.data+info_offset, buildinfo, buildinfo_size);
  info_offset = (info_offset & offset_mask) + offset_base;
  tbl_offset = (tbl_offset & offset_mask) + offset_base;
  info_offset = vendian32(endian, info_offset);
  tbl_offset = vendian32(endian, tbl_offset);
  max_id = vendian16(endian, max_id);

  fix.data[tail_offset+0x6] = (max_id >> 0) & 0xFF;
  fix.data[tail_offset+0x7] = (max_id >> 8) & 0xFF;
  fix.data[tail_offset+0x8] = (info_offset >> 0) & 0xFF;
  fix.data[tail_offset+0x9] = (info_offset >> 8) & 0xFF;
  fix.data[tail_offset+0xA] = (info_offset >>16) & 0xFF;
  fix.data[tail_offset+0xB] = (info_offset >>24) & 0xFF;
  fix.data[tail_offset+0xC] = (tbl_offset >> 0) & 0xFF;
  fix.data[tail_offset+0xD] = (tbl_offset >> 8) & 0xFF;
  fix.data[tail_offset+0xE] = (tbl_offset >>16) & 0xFF;
  fix.data[tail_offset+0xF] = (tbl_offset >>24) & 0xFF;

  FILE *ofp = fopen(argv[1], "wb");
  switch(s_fmt_type) {
    // Freya and PC can _only_ provide FIX
    case FmtType_ROM:
    case FmtType_WWSft:
      for(size_t i = 0; i < exbank_count; i++)
        prv_writebank(&exbanks[i], ofp);
      break;
  }
  prv_writebank(&fix, ofp);
  fclose(ofp);

  return 0;
}
