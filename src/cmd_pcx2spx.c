#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"
#include "pcg.h"

// in.pcx in.dat out.spx

typedef struct {
  uint16_t vx;  // Relative X coordinate
  uint16_t vy;  // Relative Y coordinate
  uint16_t pt;  // Sprite pattern number
  uint16_t rv;  // Reversal code (flipping?)
} __attribute__((__packed__)) XOBJ_FRM_DAT;

typedef struct {
  uint16_t num; // Sprite count for this composite sprite
  uint32_t ptr; // Pointer to the first XOBJ_FRM_DAT
  uint16_t unused; // Unused
} __attribute__((__packed__)) XOBJ_REF_DAT;

#define FRAME_FLAG_ROTATE (1 << 0)
typedef struct TextureFrameData {
  double u[2], v[2];
  uint16_t w, h;
  uint16_t flag;
  uint16_t palette;
} TextureFrameData;

typedef struct TextureRegistryKey {
  uint32_t frame_count;
  TextureFrameData *frames;
} TextureRegistryKey;

typedef struct TextureRegistry {
  uint32_t dat_keys, dat_frames;
  TextureFrameData atlas_frame;
  uint32_t key_count;
  TextureRegistryKey *keys;
} TextureRegistry;

typedef struct SpxHdr {
  uint32_t frm_offset;
  uint32_t frm_count;
  uint32_t ref_offset;
  uint32_t ref_count;
  uint32_t key_offset;
  uint32_t key_count;
  uint32_t pcg_offset;
  uint32_t pcg_count;
  // frm data
  // ref data
  // key data
  // pcg data
} __attribute__((__packed__)) SpxHdr;

static void prv_write32(FILE *fp, uint32_t v) {
  v = endian32(NULL, v);
  fwrite(&v, sizeof(uint32_t), 1, fp);
}
#define WRITE32(fp, v) prv_write32(fp, v);
static void prv_write16(FILE *fp, uint16_t v) {
  v = endian16(NULL, v);
  fwrite(&v, sizeof(uint16_t), 1, fp);
}
#define WRITE16(fp, v) prv_write16(fp, v);

static void prv_write_refs(FILE *fp, XOBJ_REF_DAT *refs, uint32_t count) {
  for(uint32_t i = 0; i < count; i++) {
    WRITE16(fp, refs[i].num);
    WRITE32(fp, refs[i].ptr);
    WRITE16(fp, refs[i].unused);
  }
}

static void prv_write_frms(FILE *fp, XOBJ_FRM_DAT *frms, uint32_t count) {
  for(uint32_t i = 0; i < count; i++) {
    WRITE16(fp, frms[i].vx);
    WRITE16(fp, frms[i].vy);
    WRITE16(fp, frms[i].pt);
    WRITE16(fp, frms[i].rv);
  }
}

static void prv_write_keypats(FILE *fp, uint32_t *pats, uint32_t count) {
  for(uint32_t i = 0; i < count; i++) {
    WRITE32(fp, pats[i]);
  }
}

static void prv_write_pcg(FILE *fp, uint8_t *pcg, uint32_t count) {
  // TODO: Swizzle
  for(uint32_t i = 0; i < count; i++) {
    uint8_t newpcg[16*16*4/8];
    int off = 0;
    for(int l = 0; l < 2; l++) {
      for(int y = 0; y < 16; y++) {
        memcpy(newpcg+off, pcg+(y*8), 4);
        off += 4;
      }
      pcg += 4;
    }
    pcg -= 8;
    fwrite(newpcg, 1, 16*16*4/8, fp);
    pcg += 16*16*4/8;
  }
}

static TextureRegistry *prv_load_dat(const char *fn) {
  FILE *fp = fopen(fn, "rb");

  uint32_t dat_keys, dat_frames;
  fread(&dat_keys, 4, 1, fp);
  fread(&dat_frames, 4, 1, fp);

  TextureRegistry *reg = malloc(sizeof(TextureRegistry) +
      (sizeof(TextureRegistryKey) * dat_keys) +
      (sizeof(TextureFrameData) * dat_frames));

#define FIXUP_OFFSET(var) do { var = (void*)(((uintptr_t)&(var)) + ((uintptr_t)(var))); } while(0)
#define SERIAL_VAR(var) fread(&(var), sizeof(var), 1, fp);
#define SERIAL_OFFSET(var) do { SERIAL_VAR(var); FIXUP_OFFSET(var); } while(0)
  SERIAL_VAR(reg->atlas_frame);
  SERIAL_VAR(reg->key_count);
  SERIAL_OFFSET(reg->keys);
  reg->keys = (void*)(((uintptr_t)&reg->keys) + sizeof(void*));
  for(uint32_t i = 0; i < reg->key_count; i++) {
    SERIAL_VAR(reg->keys[i].frame_count);
    SERIAL_OFFSET(reg->keys[i].frames);
  }
  TextureFrameData *frmptr = (TextureFrameData*)(reg->keys + reg->key_count);
  for(uint32_t i = 0; i < reg->key_count; i++) {
    reg->keys[i].frames = frmptr;
    frmptr += reg->keys[i].frame_count;
    for(uint32_t l = 0; l < reg->keys[i].frame_count; l++) {
      SERIAL_VAR(reg->keys[i].frames[l]);
    }
  }
  fclose(fp);

  return reg;
}

static int prv_has_hash(uint32_t *hashes, size_t max, uint32_t hash, uint16_t *til) {
  for(size_t i = 0; i < max; i++) {
    if(hashes[i] != hash)
      continue;
    *til = i;
    return 1;
  }
  return 0;
}

static uint32_t prv_some_shit(PcxFile *pcx, GameDef *game, uint8_t *tile_data, uint16_t *map) {
  size_t tiles_w = pcx->w / game->hwdef->pcg_w;
  size_t tiles_h = pcx->h / game->hwdef->pcg_h;
  size_t tile_count = tiles_w * tiles_h;
  size_t tile_size = pcg_tile_size(game);

  uint32_t empty_tile_hash = hash32(tile_data, tile_size);
  uint32_t *hashes = calloc(sizeof(uint32_t), tile_count);

  uint8_t *hash_ptr = calloc(1, tile_size);
  uint8_t *ptr = tile_data;
  size_t data_idx = 0, pos_idx = 0;
  for(size_t y = 0; y < tiles_h; y++) {
    for(size_t x = 0; x < tiles_w; x++) {
      size_t px = x, py = y;

      size_t rx = px * game->hwdef->pcg_w;
      size_t ry = py * game->hwdef->pcg_h;

      uint32_t pal;
      pal = pcg_build_tile(ptr, game, pcx, rx, ry, 0, 0, 0);
      (void)pal;

      uint16_t attr, til;
      // Handle map stuff
      uint32_t hash = hash32(ptr, tile_size);
      if(hash == empty_tile_hash) {
        attr = 0xFFFF;
        til = 0xFFFF;
        goto _emit;
      }
      attr = 0;
      if(prv_has_hash(hashes,data_idx, hash, &til))
        goto _emit;

      pcg_build_tile(hash_ptr, game, pcx, rx, ry, 1, 0, 0);
      attr = game->hwdef->map_yflip_bit;
      if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
        goto _emit;

      pcg_build_tile(hash_ptr, game, pcx, rx, ry, 0, 1, 0);
      attr = game->hwdef->map_xflip_bit;
      if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
        goto _emit;

      pcg_build_tile(hash_ptr, game, pcx, rx, ry, 1, 1, 0);
      attr = game->hwdef->map_xflip_bit | game->hwdef->map_yflip_bit;
      if(prv_has_hash(hashes,data_idx, hash32(hash_ptr, tile_size), &til))
        goto _emit;

      hashes[data_idx] = hash;

      ptr += tile_size;

      attr = 0;
      til = data_idx;
      data_idx++;
_emit:
      attr ^= game->hwdef->map_neutral_bit;
      map[pos_idx] = til;
      pos_idx++;
      map[pos_idx] = attr;
      pos_idx++;
    }
  }
  return data_idx;
}

int cmd_pcx2spx(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin = argv[0];
  const char *arg_datin = argv[1];
  const char *arg_spxout = argv[2];

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;
  if(pcx.bpp != 8) return 1;

  FILE *ofp = fopen(arg_spxout, "wb");
  SpxHdr hdr = {
    .frm_offset = sizeof(SpxHdr),
    .frm_count = 0,
    .ref_offset = 0,
    .ref_count = 0,
    .key_offset = 0,
    .key_count = 0,
    .pcg_offset = 0,
    .pcg_count = 0,
  };

  TextureRegistry *reg = prv_load_dat(arg_datin);

  for(uint32_t i = 0; i < reg->key_count; i++) {
    hdr.ref_count += reg->keys[i].frame_count;
  }

  GameDef *game = fmt_get("x68k_16x16_4bpp");

  size_t tiles_w = pcx.w / game->hwdef->pcg_w;
  size_t tiles_h = pcx.h / game->hwdef->pcg_h;

  size_t tile_count = tiles_w * tiles_h;
  size_t tile_size = pcg_tile_size(game);
  uint8_t *tile_data = calloc(tile_count, tile_size);
  uint16_t *map = calloc(sizeof(uint16_t) * 2, tile_count);

  hdr.pcg_count = prv_some_shit(&pcx, game, tile_data, map);
  hdr.key_count = reg->key_count;

  size_t ref_size = align(16, hdr.ref_count * sizeof(XOBJ_REF_DAT));
  size_t key_size = align(16, hdr.key_count * sizeof(uint32_t));
  size_t pcg_size = align(16, hdr.pcg_count * tile_size);

  XOBJ_REF_DAT *ref_base = calloc(sizeof(XOBJ_REF_DAT), hdr.ref_count);

  uint32_t *keypats = calloc(sizeof(uint32_t), reg->key_count+1);

  uint32_t frm_ptr = 0;
  XOBJ_REF_DAT *refs = ref_base;

  fseek(ofp, hdr.frm_offset + 4, SEEK_SET);
  for(uint32_t i = 0; i < reg->key_count; i++) {
    keypats[i+1] = keypats[i];
    for(uint32_t l = 0; l < reg->keys[i].frame_count; l++) {
      TextureFrameData frame = reg->keys[i].frames[l];
      uint16_t rv = 0;
      frame.u[0] *= pcx.w;
      frame.u[1] *= pcx.w;
      frame.v[0] *= pcx.h;
      frame.v[1] *= pcx.h;
      // Undo UV flipping and apply RV codes
      if(frame.u[0] > frame.u[1]) {
        rv ^= 0x4000;
        double tmp = frame.u[0];
        frame.u[0] = frame.u[1];
        frame.u[1] = tmp;
      }
      if(frame.v[0] > frame.v[1]) {
        rv ^= 0x8000;
        double tmp = frame.v[0];
        frame.v[0] = frame.v[1];
        frame.v[1] = tmp;
      }
      int xtiles = frame.w / game->hwdef->pcg_w;
      int ytiles = frame.h / game->hwdef->pcg_h;
      int tile_x = frame.u[0] / game->hwdef->pcg_w;
      int tile_y = frame.v[0] / game->hwdef->pcg_h;
      int vx = -frame.w / 2;
      int vy = -frame.h / 2;

      refs->num = xtiles * ytiles;
      refs->ptr = frm_ptr;
      for(int yt = 0; yt < ytiles; yt++, vy += game->hwdef->pcg_h) {
        for(int xt = 0; xt < xtiles; xt++, vx += game->hwdef->pcg_w) {
          int pt = (tile_x + xt) + ((tile_y + yt) * tiles_w);
          int til = map[(pt * 2) + 0];
          int attr = map[(pt * 2) + 1];
          if(til == 0xFFFF) {
            refs->num--;
            continue;
          }
          XOBJ_FRM_DAT frm;
          frm.vx = vx;
          frm.vy = vy;
          frm.pt = til;
          frm.rv = rv ^ attr;
          prv_write_frms(ofp, &frm, 1);
          hdr.frm_count++;
        }
      }
      frm_ptr += refs->num * sizeof(XOBJ_FRM_DAT);
      refs++;
      keypats[i+1]++;
    }
  }
  size_t frm_size = align(16, hdr.frm_count * sizeof(XOBJ_FRM_DAT));

  hdr.ref_offset = hdr.frm_offset + frm_size;
  hdr.key_offset = hdr.ref_offset + ref_size;
  hdr.pcg_offset = hdr.key_offset + key_size;
  uint32_t file_size = hdr.pcg_offset + pcg_size;

  fseek(ofp, 0, SEEK_SET);
  WRITE32(ofp, file_size);
  WRITE32(ofp, hdr.frm_offset);
  WRITE32(ofp, hdr.frm_count);
  WRITE32(ofp, hdr.ref_offset);
  WRITE32(ofp, hdr.ref_count);
  WRITE32(ofp, hdr.key_offset);
  WRITE32(ofp, hdr.key_count);
  WRITE32(ofp, hdr.pcg_offset);
  WRITE32(ofp, hdr.pcg_count);

  fseek(ofp, hdr.ref_offset + 4, SEEK_SET);
  prv_write_refs(ofp, ref_base, hdr.ref_count);
  fseek(ofp, hdr.key_offset + 4, SEEK_SET);
  prv_write_keypats(ofp, keypats, hdr.key_count);
  fseek(ofp, hdr.pcg_offset + 4, SEEK_SET);
  prv_write_pcg(ofp, tile_data, hdr.pcg_count);

  fclose(ofp);
  return 0;
}
