#ifndef PCG_H_
#define PCG_H_

// returns the palette used
uint32_t pcg_build_tile(void *tile, GameDef *fmt, PcxFile *pcx, size_t src_x, size_t src_y,
                        int flipx, int flipy, int rot);
// Tile size in bytes
size_t pcg_tile_size(GameDef *fmt);

void flip_perform(size_t *x, size_t *y, size_t max_w, size_t max_h,
                  int flipx, int flipy, int rot);

#endif
