#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "wav.h"

static int32_t prv_read_sample(WavFile *wav, size_t *ri) {
  int32_t sample = 0;
  switch(wav->samplebits) {
    case 8:
      sample = (((uint8_t*)wav->data)[*ri] - 0x80) << 24;
      break;
    case 16:
      sample = ((uint16_t*)wav->data)[*ri] << 16;
      break;
    case 24: // TODO: verify
      sample = ((uint32_t*)wav->data)[*ri] << 8;
      break;
    case 32:
      sample = ((uint32_t*)wav->data)[*ri];
      break;
  }
  (*ri)++;
  return sample;
}

// format is something like u16, s8, s32
int cmd_wavconv(int argc, const char *argv[]) {
  (void)argc;
  int to_unsigned, to_samplebits, to_samplerate, to_channels;

  const char *arg_wavin = argv[0];
  char *arg_format_ptr = strdup(argv[1]);
  char *arg_format = arg_format_ptr;
  to_samplerate = strtol(argv[2], NULL, 0);
  const char *arg_outbin = argv[3];

  to_unsigned = 0;
  to_samplebits = 0;
  to_channels = 1;
  for(; *arg_format != '\0'; arg_format++) {
    switch(*arg_format) {
      case 's': case 'u':
        to_unsigned = *arg_format == 'u' ? 1 : 0;
        arg_format++;
        to_samplebits = strtol(arg_format, &arg_format, 0);
        break;
      case 'x': case '*':
        arg_format++;
        to_channels = strtol(arg_format, &arg_format, 0);
        break;
    }
  }
  free(arg_format_ptr);

  WavFile wav;
  if(!wav_new(&wav, arg_wavin))
    return 1;

  size_t to_samples = wav.samples * to_samplerate / wav.samplerate;

  dbgprint("Channels: %d -> %d\n", wav.channels, to_channels);
  dbgprint("SampleRate: %d -> %d\n", wav.samplerate, to_samplerate);
  dbgprint("Bits: %d -> %d\n", wav.samplebits, to_samplebits);
  dbgprint("Sample count: %zu -> %zu\n", wav.samples, to_samples);

  if(wav.channels != to_channels) {
    // TODO: Make this work; at the least, make it mix-down 2ch to 1ch.
    errprint("Channel count doesn't match, results probably wrong.\n");
  }

  int32_t **in_samples;
  in_samples = calloc(sizeof(int32_t*), wav.channels);
  for(int ch = 0; ch < wav.channels; ch++) {
    in_samples[ch] = calloc(sizeof(int32_t), wav.samples);
  }

  size_t ri = 0;
  for(size_t i = 0; i < wav.samples; i++) {
    for(int ch = 0; ch < wav.channels; ch++) {
      in_samples[ch][i] = prv_read_sample(&wav, &ri);
    }
  }

  int32_t **out_samples;
  out_samples = calloc(sizeof(int32_t*), to_channels);
  for(int ch = 0; ch < to_channels; ch++) {
    out_samples[ch] = calloc(sizeof(int32_t), to_samples);
  }

  size_t srcidx = 0;
  for(size_t i = 0; i < to_samples; i++) {
    size_t nextidx = (i+1) * wav.samplerate / to_samplerate;
    size_t step = nextidx - srcidx;
    for(int ch = 0; ch < to_channels; ch++) {
      int64_t tmp = 0;
      // Average skipped samples to make output sample
      for(size_t l = 0; l < step; l++) {
        tmp += in_samples[ch][srcidx+l];
      }
      out_samples[ch][i] = tmp / (int64_t)step;
    }
    srcidx = nextidx;
  }

  FILE *fp = fopen(arg_outbin, "wb");
  uint8_t tmp[4];
  for(size_t i = 0; i < to_samples; i++) {
    for(int ch = 0; ch < to_channels; ch++) {
      if(to_unsigned)
        out_samples[ch][i] ^= 0x80000000; // Doesn't cause signed overflow
      switch(to_samplebits) {
        case 8:
          tmp[0] = out_samples[ch][i] >> 24;
          fwrite(tmp, 1, 1, fp);
          break;
        case 16:
          tmp[0] = out_samples[ch][i] >> 16;
          tmp[1] = out_samples[ch][i] >> 24;
          fwrite(tmp, 2, 1, fp);
          break;
        case 32:
          tmp[0] = out_samples[ch][i] >> 0;
          tmp[1] = out_samples[ch][i] >> 8;
          tmp[2] = out_samples[ch][i] >> 16;
          tmp[3] = out_samples[ch][i] >> 24;
          fwrite(tmp, 4, 1, fp);
          break;
      }
    }
  }
  fclose(fp);

  return 0;
}
