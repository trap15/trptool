#ifndef FORMAT_H_
#define FORMAT_H_

typedef struct {
  const char *name;
  size_t offset; // offset into the source file in bytes
  size_t size; // number of words to copy
  size_t wordsize; // how many bytes per word
  size_t wordskip; // how many words to skip after each copied word
  size_t addr_xor; // XOR on the address
} RomSplitOut;

typedef struct {
  RomSplitOut outputs[16];
} RomSplit;

typedef struct {
  const char *name; // hardware name
  const char *inherit_name; // hardware to inherit from

  // memory
  int endian; // (-1=little, 1=big)

  // pcg (hardware tile)
  size_t pcg_bpp; // bits per pixel
  size_t pcg_w, pcg_h; // resolution
  int pcg_planar; // is planar (-1=chunky, 1=planar)
  int pcg_bit_flip; // bits are 'flipped' order
                    // planar: -1=byte0 is LSB, 1=byte0 is MSB
                    // chunky: -1=bit0 is LSB, 1=bit0 is MSB
  int pcg_pix_flip; // pixels are 'flipped' order
                    // planar: -1=bit0 is left-most, 1=bit0 is right-most
                    // chunky: -1=byte0 is left-most, 1=byte0 is right-most
  int pcg_xflip; // pixels are flipped X-wise
  int pcg_yflip; // pixels are flipped Y-wise

  // palette
  size_t pal_size; // colors per palette
  size_t pal_count; // palette count
  size_t pal_depth; // bits per palette entry
  int pal_r_size, pal_r_shift;
  int pal_g_size, pal_g_shift;
  int pal_b_size, pal_b_shift;

  // tilemap
  int map_neutral_bit;
  int map_xflip_bit;
  int map_yflip_bit;
  int map_pal_bit;
  int map_rotate;
  int map_emptyval;

  // romsplit
  RomSplit roms[8];
} HardwareDef;

typedef struct {
  const char *name;
  const char *hwdef_name;
  HardwareDef *hwdef; // Do not fill in def!
} GameDef;

GameDef *fmt_get(const char *name);

uint16_t endian16(GameDef *game, uint16_t val);
uint32_t endian32(GameDef *game, uint32_t val);

#endif
