#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// SKYLINE-BL algorithm

// out framedata.dat flags in1 [in2]

#define FRAME_FLAG_ROTATE (1 << 0)
typedef struct TextureFrameData {
  double u[2], v[2];
  uint16_t w, h;
  uint16_t flag;
  uint16_t palette;
} TextureFrameData;

typedef struct TextureRegistryKey {
  uint32_t frame_count;
  TextureFrameData *frames;
} TextureRegistryKey;

typedef struct TextureRegistry {
  uint32_t dat_keys, dat_frames;
  TextureFrameData atlas_frame;
  uint32_t key_count;
  TextureRegistryKey *keys;
} TextureRegistry;

typedef struct RawTextureFrameData {
  uint32_t u[2], v[2];
  uint16_t w, h;
  uint16_t palette;
} RawTextureFrameData;

int skyline_height(int *skyline, int x, int width) {
  int height = 0;
  for(int i = 0; i < width; i++) {
    if(skyline[x+i] > height) {
      height = skyline[x+i];
    }
  }
  return height;
}

void skyline_set_height(int *skyline, int x, int width, int height) {
  for(int i = 0; i < width; i++) {
    skyline[x+i] = height;
  }
}

#define TRANSFORM_XFLIP (1 << 0)
#define TRANSFORM_YFLIP (1 << 1)
#define TRANSFORM_ROTATE (1 << 2)
#define TRANSFORM_ALL (TRANSFORM_XFLIP | TRANSFORM_YFLIP | TRANSFORM_ROTATE)

typedef struct FrameTrackingInfo {
  const char *filename;
  int frameidx;
  int transform;
  int key, frame;
} FrameTrackingInfo;

int track_comparator(const void *l_, const void *r_) {
  const FrameTrackingInfo *l = (const FrameTrackingInfo*)l_;
  const FrameTrackingInfo *r = (const FrameTrackingInfo*)r_;
  if(l->key != r->key)
    return l->key - r->key;
  else if(l->frame != r->frame)
    return l->frame - r->frame;
  else{
    printf("duplicate key-frame pair!! %s v %s\n", l->filename, r->filename);
    exit(7);
  }
}

static void prv_get_key_frame_from_fn(const char *fname, int *key, int *frame) {
  const char *metadata = strrchr(fname, '/');
  if(metadata == NULL) {
    metadata = fname;
  }else{
    metadata++;
  }

  char *next;
  int val = strtol(metadata, &next, 10);
  if(next == metadata) {
    printf("missing key %s\n", fname);
    exit(3);
  }
  metadata = next;
  if(*metadata != '~') {
    printf("missing key seperator %s\n", fname);
    exit(4);
  }
  metadata++;
  *key = val;

  val = strtol(metadata, &next, 10);
  if(next == metadata) {
    printf("missing frame %s\n", fname);
    exit(5);
  }
  metadata = next;
  if(*metadata != '.') {
    printf("missing value seperator %s\n", fname);
    exit(6);
  }
  metadata++;
  *frame = val;
}

void get_key_frame_from_filename(FrameTrackingInfo *info) {
  prv_get_key_frame_from_fn(info->filename, &info->key, &info->frame);
}

int pcxname_comparator(const void *l_, const void *r_) {
  const char **l = *(const char***)l_;
  const char **r = *(const char***)r_;
  int lkey, lframe;
  int rkey, rframe;

  prv_get_key_frame_from_fn(*l, &lkey, &lframe);
  prv_get_key_frame_from_fn(*r, &rkey, &rframe);
  if(lkey != rkey)
    return lkey - rkey;
  else if(lframe != rframe)
    return lframe - rframe;
  else{
    printf("duplicate key-frame pair!! %s v %s\n", *l, *r);
    exit(7);
  }
}

int cmd_pcxatlas(int argc, char *argv[]) {
  const char *arg_pcxout = argv[0];
  const char *arg_datout = argv[1];
  const char *arg_flags = argv[2];
  if(strcmp(arg_pcxout, arg_datout) == 0)
    return 0;
  const char **arg_raw_pcxins = (const char**)argv + 3;
  const int framecount = argc - 3;
  const char ***arg_sorting_pcxins = calloc(sizeof(const char **), framecount);
  const char **arg_pcxins = calloc(sizeof(const char *), framecount);
  for(int i = 0; i < framecount; i++) {
    arg_sorting_pcxins[i] = &arg_raw_pcxins[i];
  }
  RawTextureFrameData *frames = calloc(sizeof(RawTextureFrameData), framecount);
  PcxFile *pcxs = calloc(sizeof(PcxFile), framecount);
  FrameTrackingInfo *tracks = calloc(sizeof(FrameTrackingInfo), framecount);
  int pcxcount = 0;
  int flags = strtol(arg_flags, NULL, 0);
  int tilesize = flags & 0xFF;
  int can_rotate = !(flags & 0x100);

  qsort(arg_sorting_pcxins, framecount, sizeof(const char**), pcxname_comparator);
  for(int i = 0; i < framecount; i++) {
    arg_pcxins[i] = *(arg_sorting_pcxins[i]);
  }

  for(int i = 0; i < framecount; i++, pcxcount++) {
    if(!pcx_new(&pcxs[pcxcount], arg_pcxins[i]))
      return 1;
    int bytes = pcxs[pcxcount].stride * pcxs[pcxcount].h;
    tracks[i] = (FrameTrackingInfo){
      .filename = arg_pcxins[i],
      .frameidx = pcxcount,
      .transform = 0,
      .key = -1, .frame = -1,
    };
    get_key_frame_from_filename(&tracks[i]);
    for(int l = pcxcount - 1; l >= 0; l--) {
      int early = 0;
      int max_transform = TRANSFORM_XFLIP | TRANSFORM_YFLIP | (can_rotate ? TRANSFORM_ROTATE : 0);
      for(int transform = 0; transform <= max_transform; transform++) {
        int sw = pcxs[pcxcount].w;
        int sh = pcxs[pcxcount].h;
        int nw = sw;
        int nh = sh;
        if(transform & TRANSFORM_ROTATE) {
          int tmp = nw;
          nw = nh;
          nh = tmp;
        }
        if(nw != pcxs[l].w ||
           nh != pcxs[l].h)
          continue;
        uint8_t *newdata = pcxs[pcxcount].data;
        uint8_t *olddata = pcxs[l].data;
        int passed = 1;
        if(transform == 0) {
          passed = (memcmp(newdata, olddata, bytes) == 0);
        }else{
          for(int y = 0; y < sh && passed; y++) {
            int ry = (transform & TRANSFORM_YFLIP) ? sh - y - 1 : y;
            for(int x = 0; x < sw && passed; x++) {
              int rx = (transform & TRANSFORM_XFLIP) ? sw - x - 1 : x;
              const int newpos = rx + (ry * sw);
              const int oldpos = (transform & TRANSFORM_ROTATE) ?
                                  y + (x * nw) :
                                  x + (y * nw);
              uint8_t oldpx = olddata[oldpos] & 0xF;
              uint8_t newpx = newdata[newpos] & 0xF;
              if(oldpx != newpx)
                passed = 0;
            }
          }
        }
        if(!passed)
          continue;
        tracks[i].frameidx = l;
        tracks[i].transform = transform;
        early = 1;
        break;
      }
      if(early) {
        free(pcxs[pcxcount].data);
        pcxs[pcxcount].data = NULL;
        pcxcount--;
        break;
      }
    }
  }

  int *skyline = NULL;
  PcxFile opcx;
  memset(&opcx, 0, sizeof(opcx));

  // start at 32*32
  int last_dimension = 32/2;

attempt:
  if(opcx.data) {
    free(opcx.data);
  }
  if(skyline) {
    free(skyline);
  }
  last_dimension *= 2;
  opcx.bpp = 8;
  opcx.w = last_dimension;
  opcx.h = last_dimension;
  opcx.stride = opcx.w * opcx.bpp / 8;
  opcx.data = calloc(opcx.h, opcx.stride);
  opcx.pal = NULL; // TODO

  skyline = calloc(sizeof(int), opcx.w);

  for(int i = 0; i < pcxcount; i++) {
    PcxFile *pcx = &pcxs[i];
    if(opcx.pal == NULL) opcx.pal = pcx->pal;
    int fit_w = align(tilesize, pcx->w);
    int fit_h = align(tilesize, pcx->h);
    frames[i].w = fit_w;
    frames[i].h = fit_h;
    int x = 0;
    int y = 0;
    int best_x_setting = -1;
    int best_x_value = opcx.h - fit_h + 1;
    while(x + fit_w <= opcx.w) {
      y = skyline_height(skyline, x, fit_w);
      if(y < best_x_value) {
        best_x_setting = x;
        best_x_value = y;
      }
      x += tilesize;
    }
    if(best_x_setting == -1) { // no fit
      goto attempt;
    }
    x = best_x_setting;
    y = best_x_value;

    frames[i].u[0] = x;
    frames[i].u[1] = x + fit_w;
    frames[i].v[0] = y;
    frames[i].v[1] = y + fit_h;
    for(int yi = 0; yi < pcx->h; yi++) {
      memcpy(opcx.data + ((y+yi) * opcx.stride) + x, pcx->data + (yi * pcx->stride), pcx->stride);
    }
    skyline_set_height(skyline, x, fit_w, y + fit_h);
  }

  pcx_save(&opcx, arg_pcxout);

  qsort(tracks, framecount, sizeof(FrameTrackingInfo), track_comparator);

  TextureRegistry texreg = {
    .atlas_frame = {
      .u = {0.0, 1.0},
      .v = {0.0, 1.0},
      .w = opcx.w, .h = opcx.h,
      .flag = 0,
      .palette = 0,
    },
    .key_count = tracks[framecount-1].key + 1,
  };
  texreg.keys = calloc(texreg.key_count, sizeof(TextureRegistryKey));

  int last_key = -1;
  texreg.dat_keys = 0;
  texreg.dat_frames = 0;
  for(int i = 0; i < framecount; i++) {
    if(tracks[i].key != last_key) {
      if(last_key != -1) {
        texreg.dat_frames += texreg.keys[last_key].frame_count;
        texreg.keys[last_key].frames = calloc(texreg.keys[last_key].frame_count, sizeof(TextureFrameData));
      }
      last_key = tracks[i].key;
      texreg.keys[last_key].frame_count = 0;
    }
    texreg.keys[last_key].frame_count++;
  }
  texreg.dat_frames += texreg.keys[last_key].frame_count;
  texreg.keys[last_key].frames = calloc(texreg.keys[last_key].frame_count, sizeof(TextureFrameData));
  texreg.dat_keys = last_key + 1;

  last_key = -1;
  int last_frame = -1;
  for(int i = 0; i < framecount; i++) {
    if(tracks[i].key != last_key) {
      last_key = tracks[i].key;
    };
    RawTextureFrameData *frame = &frames[tracks[i].frameidx];
    int transform = tracks[i].transform;
    int flags = 0;
    double ul,ur, vt,vb;
    int w = frame->w;
    int h = frame->h;
    ul = frame->u[0]; ur = frame->u[1];
    vt = frame->v[0]; vb = frame->v[1];
    ul /= (double)opcx.w;
    ur /= (double)opcx.w;
    vt /= (double)opcx.h;
    vb /= (double)opcx.h;
    if((ul * opcx.w != frame->u[0]) || (ur * opcx.w != frame->u[1]) ||
       (vt * opcx.h != frame->v[0]) || (vb * opcx.h != frame->v[1])) {
      errprint("floating point inaccuracy detected on frame %d!\n", i);
    }

    if(transform & TRANSFORM_XFLIP) {
      double tmp = ul; ul = ur; ur = tmp;
    }
    if(transform & TRANSFORM_YFLIP) {
      double tmp = vt; vt = vb; vb = tmp;
    }
    if(transform & TRANSFORM_ROTATE) {
      int tmp = w; w = h; h = tmp;
      flags |= FRAME_FLAG_ROTATE;
    }
    texreg.keys[last_key].frames[tracks[i].frame] = (TextureFrameData) {
      .u = {ul, ur},
      .v = {vt, vb},
      .w = w,
      .h = h,
      .flag = flags,
      .palette = 0,
    };
    last_frame = tracks[i].frame;
  }
  (void)last_frame;

  FILE *fp = fopen(arg_datout, "wb");
  void *ptr = NULL;
  TextureRegistry *reg = &texreg;
#define SERIAL_VAR(var) fwrite(&(var), sizeof(var), 1, fp);
#define SERIAL_OFFSET(var) do { ptr = NULL; fwrite(&ptr, 4, 1, fp); } while(0)
  SERIAL_VAR(reg->dat_keys);
  SERIAL_VAR(reg->dat_frames);
  SERIAL_VAR(reg->atlas_frame);
  SERIAL_VAR(reg->key_count);
  SERIAL_OFFSET(reg->keys);
  for(uint32_t i = 0; i < reg->key_count; i++) {
    SERIAL_VAR(reg->keys[i].frame_count);
    SERIAL_OFFSET(reg->keys[i].frames);
  }
  for(uint32_t i = 0; i < reg->key_count; i++) {
    for(uint32_t l = 0; l < reg->keys[i].frame_count; l++) {
      SERIAL_VAR(reg->keys[i].frames[l]);
    }
  }
  fclose(fp);

  return 0;
}

