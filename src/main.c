#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"

typedef struct {
  const char *name;
  int min, max;
  const char *help_str;
  int (*func)(int argc, const char *argv[]);
} CommandDef;

#define COMMAND_DEF(name, min, max, help_str) \
int cmd_##name(int argc, const char *argv[]);
#include "commands.def"
#undef COMMAND_DEF

static CommandDef s_commands[] = {
#define COMMAND_DEF(name, min, max, help_str) \
  {#name, min, max, help_str, cmd_##name},
#include "commands.def"
#undef COMMAND_DEF
  {NULL,0,0,NULL,NULL},
};

static const char *s_app;
static CommandDef *s_cmd;

void usage(void) {
  errprint("Usage:\n\t%s ", s_app);
  if(s_cmd != NULL) {
    errprint("%s %s", s_cmd->name, s_cmd->help_str);
    // TODO
  }else{
    errprint("[command]");
  }
  errprint("\n");
}

int main(int argc, const char *argv[]) {
  s_app = argv[0];
  s_cmd = NULL;
  if(argc < 2) {
    usage();
    return EXIT_FAILURE;
  }
  for(s_cmd = s_commands; s_cmd; s_cmd++) {
    if(strcmp(s_cmd->name, argv[1]) == 0) {
      break;
    }
  }
  if(s_cmd == NULL) {
    s_cmd = NULL;
    usage();
    return EXIT_FAILURE;
  }

  argc -= 2;
  argv += 2;
  if(argc < s_cmd->min || (argc > s_cmd->max && s_cmd->max >= 0)) {
    usage();
    return EXIT_FAILURE;
  }
  if(s_cmd->func(argc, argv)) {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
