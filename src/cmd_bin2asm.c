#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"

// in out

int cmd_bin2asm(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_inbin = argv[0];
  const char *arg_outasm = argv[1];

  FILE *ifp = fopen(arg_inbin, "rb");
  FILE *ofp = fopen(arg_outasm, "wb");
  int idx = 0;
  while(!feof(ifp)) {
    uint8_t v;
    if(fread(&v, 1, 1, ifp) != 1)
      break;
    if(idx == 0) {
      fprintf(ofp, "\tDB\t");
    }else{
      fprintf(ofp, ", ");
    }
    fprintf(ofp, "0%02Xh", v);
    idx++; idx %= 16;
    if(idx == 0)
      fprintf(ofp, "\r\n");
  }
  fprintf(ofp, "\r\n");

  fclose(ifp);
  fclose(ofp);

  return 0;
}
