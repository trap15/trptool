#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "lze.h"

#define CMD_DECODE    0x00       // decode
#define CMD_CODE_LE   0x654C     // LZE magic number

#define LZE_SHIFT     2          // bits to shift
#define LZE_MASK      0x03       // first bits to check, ((1 << LZE_SHIFT) - 1)
#define LZE_LZS4C     0x0        // 00 binary, short LZ
#define LZE_LZS62     0x1        // 01 binary, normal LZ
#define LZE_COPY1     0x2        // 10 binary, short LZ
#define LZE_COPYN     0x3        // 11 binary, normal LZ

#define LZE_COPYN_COUNT (LZE_N1-1)

#define LZE_LZS4C_POSBITS 12
#define LZE_LZS4C_LENBITS (16 - LZE_LZS4C_POSBITS)
#define LZE_LZS4C_POSMASK ((1 << LZE_LZS4C_POSBITS) - 1)
#define LZE_LZS62_POSBITS 2
#define LZE_LZS62_LENBITS (8 - LZE_LZS62_POSBITS)
#define LZE_LZS62_POSMASK ((1 << LZE_LZS62_POSBITS) - 1)

#define LZE_THRESHOLD (LZE_COPYN_COUNT-1)                             // max number of bytes to not encode
#define LZE_N1        ((1 << LZE_LZS62_POSBITS) + 0)                  // max offset
#define LZE_F1        ((1 << LZE_LZS62_LENBITS) + LZE_THRESHOLD - 1)  // max coded
#define LZE_N         ((1 << LZE_LZS4C_POSBITS) + LZE_N1)             // max offset
#define LZE_F         ((1 << LZE_LZS4C_LENBITS) + LZE_THRESHOLD)      // max coded

size_t lze_encode(uint8_t *dest, const uint8_t *src, size_t src_size) {
  unsigned char *pak_buffer, *pak, *raw_buffer, *raw, *raw_end, *flg, store[LZE_N1-1];
  unsigned int   len, pos, len_best, pos_best;
  unsigned int   mode, nbits, store_len;

  raw_buffer = (unsigned char *)src;
  pak_buffer = dest;

  pak = pak_buffer;
  raw = raw_buffer;
  raw_end = raw_buffer + src_size;

  nbits = 0;
  store_len = 0;

  while (raw < raw_end) {
    if (!nbits & !store_len) *(flg = pak++) = 0;

    mode = LZE_COPY1;
    len_best = LZE_THRESHOLD - 1;

    pos = raw - raw_buffer >= LZE_N1 ? LZE_N1 : raw - raw_buffer;
    for ( ; pos; pos--) {
      for (len = 0; len < LZE_F1; len++) {
        if (raw + len == raw_end) break;
        if (*(raw + len) != *(raw + len - pos)) break;
      }

      if (len > len_best) {
        mode = LZE_LZS62;
        pos_best = pos;
        if ((len_best = len) == LZE_F1) break;
      }
    }

    if (len_best < LZE_F) {
      pos = raw - raw_buffer >= LZE_N ? LZE_N : raw - raw_buffer;
      for ( ; pos > LZE_N1; pos--) {
        for (len = 0; len < LZE_F; len++) {
          if (raw + len == raw_end) break;
          if (*(raw + len) != *(raw + len - pos)) break;
        }

        if (len > len_best) {
          if (len > LZE_THRESHOLD) {
            mode = LZE_LZS4C;
            pos_best = pos - LZE_N1;
            if ((len_best = len) == LZE_F) break;
          }
        }
      }
    }

    if ((mode == LZE_LZS4C) || (mode == LZE_LZS62)) {
      unsigned char *store_ptr = store;
      while (store_len) {
        *pak++ = *store_ptr++;
        *flg |= LZE_COPY1 << nbits;
        nbits = (nbits + LZE_SHIFT) & 0x7;
        if (!nbits) *(flg = pak++) = 0;
        store_len--;
      }

      if (mode == LZE_LZS4C) {
        int npos = pos_best - 1;
        int nlen = len_best - LZE_THRESHOLD - 1;
        unsigned short value = (npos & LZE_LZS4C_POSMASK) | (nlen << LZE_LZS4C_POSBITS);
        *pak++ = value & 0xFF;
        *pak++ = value >> 8;
      } else {
        int npos = pos_best - 1;
        int nlen = len_best - LZE_THRESHOLD;
        unsigned char value = (npos & LZE_LZS62_POSMASK) | (nlen << LZE_LZS62_POSBITS);
        *pak++ = value;
      }
      *flg |= mode << nbits;
      nbits = (nbits + LZE_SHIFT) & 0x7;

      raw += len_best;
    } else {
      store[store_len++] = *raw++;
      if (store_len == LZE_COPYN_COUNT) {
        memcpy(pak, store, store_len);
        pak += store_len;
        *flg |= LZE_COPYN << nbits;
        nbits = (nbits + LZE_SHIFT) & 0x7;
        store_len = 0;
      }
    }
  }

  if (store_len) {
    *pak++ = store[0];
    *flg |= LZE_COPY1 << nbits;
    nbits = (nbits + LZE_SHIFT) & 0x7;
    if (store_len == 2) {
      if (!nbits) *(flg = pak++) = 0;
      *pak++ = store[1];
      *flg |= LZE_COPY1 << nbits;
      nbits = (nbits + LZE_SHIFT) & 0x7;
    }
  }

  *pak++ = (src_size >> 0) & 0xFF;
  *pak++ = (src_size >> 8) & 0xFF;
  *pak++ = (src_size >>16) & 0xFF;
  *pak++ = (src_size >>24) & 0xFF;

  return (size_t)(pak - pak_buffer);
}

size_t lze_decode(uint8_t *dest, const uint8_t *src, size_t src_size) {
  uint16_t flag = 0;
  size_t dst_size = 0;
  for(size_t left = src_size; left <= src_size;) {
    flag >>= 2;
    if((flag & 0xFF00) == 0) {
      flag = *(src++) | 0xFF00;
      left--;
    }
    if(flag & 2) {
      // copy
      *(dest++) = *(src++);
      left--;
      dst_size++;
      if(flag & 1) {
        *(dest++) = *(src++);
        *(dest++) = *(src++);
        dst_size += 2;
        left -= 2;
      }
    }else{
      // lzs
      uint16_t dist, size;
      size = dist = *(src++);
      left--;
      if(flag & 1) {
        // lzs62
        dist = (dist & 3) + 1;
        size = (size >> 2) + 2;
      }else{
        // lzs4c
        size = *(src++);
        left--;
        dist |= size << 8;
        dist = (dist & 0xFFF) + 5;
        size = (size >> 4) + 3;
      }
      for(uint16_t l = 0; l < size; l++) {
        *dest = *(dest-dist);
        dst_size++;
        dest++;
      }
    }
  }
  return dst_size;
}
