#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"

/* flags:
-type [ws|wsc]                                       -- defaults to ws
-fmt [rom|wwsft|freya]                               -- defaults to rom
-title [string]                                      -- defaults to "NONAME"
-fname [string]                                      -- defaults to "NONAME"
-mfgid [8bit value]                                  -- defaults to 0x00
-cartid [8bit value]                                 -- defaults to 0x00
-ver [8bit value]                                    -- defaults to 0x00
-sram [none|256K|1M|2M]                              -- defaults to none
-eeprom [none|1K|2K|4K|8K|16K|32K|64K|128K]          -- defaults to none
-orient [h|v]                                        -- defaults to h
-romtype [fast|slow]-[16|8]                          -- defaults to fast-16
-romsize [auto|1M|2M|4M|8M|16M|24M|32M|48M|64M|128M] -- defaults to auto
-entrypoint [farptr]                                 -- defaults to 0x20000000
-prgbank [last|8bit value]                           -- defaults to 0
-rtc                                                 -- defaults to not specified
*/

enum {
  WsType_WS,
  WsType_WSC,
};

enum {
  FmtType_ROM,
  FmtType_WWSft,
  FmtType_Freya,
};

const int header_sizes[] = {
  [FmtType_ROM] = 0x20,
  [FmtType_WWSft] = 0x20,
  [FmtType_Freya] = 0x80,
};

const int binary_offsets[] = {
  [FmtType_ROM] = 0,
  [FmtType_WWSft] = 0,
  [FmtType_Freya] = 0x80,
};

size_t rom_size_tbl[] = {
  1*128*1024,
  2*128*1024,
  4*128*1024,
  8*128*1024,
  16*128*1024,
  24*128*1024,
  32*128*1024,
  48*128*1024,
  64*128*1024,
  128*128*1024,
};

typedef struct {
  int ws_type;
  int outfmt;
  int romsize;
  uint8_t mfgid;
  uint8_t cartid;
  uint8_t ver;
  uint8_t save;
  uint8_t lsi;
  uint8_t flag;
  uint32_t entrypoint;
  int prgbank;
  const char *title;
  const char *freya_fname;
} Config;

uint8_t read8(void* data)
{
  uint8_t* buf = data;
  return *buf;
}
uint16_t read16(void* data)
{
  uint8_t* buf = data;
  return read8(buf) | (read8(buf+1) << 8);
}
uint32_t read32(void* data)
{
  uint8_t* buf = data;
  return read16(buf) | (read16(buf+2) << 16);
}

void write8(void* data, uint8_t val)
{
  uint8_t* buf = data;
  *buf = val;
}
void write16(void* data, uint16_t val)
{
  uint8_t* buf = data;
  write8(buf, val);
  write8(buf+1, val >> 8);
}
void write32(void* data, uint32_t val)
{
  uint8_t* buf = data;
  write16(buf, val);
  write16(buf+2, val >> 16);
}

static void prv_add_rom_hdr(uint8_t *bin, size_t bytes, Config *cfg) {
  uint8_t *hdr = bin + bytes - header_sizes[cfg->outfmt];
  memset(hdr, 0, header_sizes[cfg->outfmt]);
  size_t bank = bytes;
  if(cfg->outfmt == FmtType_WWSft) {
    bank = 4*128*1024; // WW is actually 4Mbit though
  }
  bank = 0x100 - (bank >> 16); // first bank
  switch(cfg->prgbank) {
    case -1: // last
      bank = 0xFF;
      if(cfg->outfmt == FmtType_WWSft) {
        // last bank isn't the actual last bank on WWSFT
        bank = 0xFE;
      }
      break;
    default:
      bank += cfg->prgbank;
      break;
  }
  write8(hdr+0, 0xB0); write8(hdr+1, bank); /* MOV AL, bank */
  uint8_t bankreg = 0xC2;
  switch(cfg->entrypoint & 0xF0000000) {
    case 0x20000000:
      bankreg = 0xC2;
      break;
    case 0x30000000:
      bankreg = 0xC3;
      break;
  }
  write8(hdr+2, 0xE6); write8(hdr+3, bankreg); /* OUT bankreg, AL */
  uint8_t initial_ax = 0;
  if(cfg->outfmt == FmtType_WWSft) {
    initial_ax |= 1 << 0;
  }
  write8(hdr+4, 0xB8); write16(hdr+5, initial_ax); /* MOV AX, initial_ax */
  write8(hdr+7, 0xEA); write32(hdr+8, cfg->entrypoint); /* jmp far 02000:0000h */

  write8(bin + bytes-16+0, 0xEA);
  if(cfg->outfmt == FmtType_WWSft) {
    write32(bin + bytes-16+1, 0xEFFE0000); /* jmp far 0EFFE:0000h */
  }else{
    write32(bin + bytes-16+1, 0xFFFE0000); /* jmp far 0FFFE:0000h */
  }
  write8(bin + bytes-10+0, cfg->mfgid);
  write8(bin + bytes-10+1, cfg->ws_type == WsType_WSC ? 1 : 0);
  write8(bin + bytes-10+2, cfg->cartid);
  write8(bin + bytes-10+3, cfg->ver);
  write8(bin + bytes-10+4, cfg->romsize);
  write8(bin + bytes-10+5, cfg->save);
  write8(bin + bytes-10+6, cfg->flag);
  write8(bin + bytes-10+7, cfg->lsi);

  uint16_t xsum = 0;
  for(size_t pos = 0; pos < bytes-2; pos++) {
    xsum += read8(bin + pos);
  }
  write16(bin + bytes-10+8, xsum);
}

static void prv_add_freya_hdr(uint8_t *bin, size_t bytes, Config *cfg) {
  uint8_t *hdr = bin;
  size_t rawbytes = bytes - header_sizes[cfg->outfmt];
  memset(hdr, 0xFF, header_sizes[cfg->outfmt]);
  memcpy(hdr, "#!ws", 4);
  memset(hdr+0x40, 0, 0x40);
  strncpy((char*)hdr+0x40, cfg->freya_fname, 0x10);
  strncpy((char*)hdr+0x50, cfg->title, 0x18);
  // Offset
  write32(hdr+0x68, 0);
  // Size
  write32(hdr+0x6C, rawbytes);
  // Number of blocks
  write16(hdr+0x70, bytes >> 7);
  // Flags
  write16(hdr+0x72, 7);
  // Modify time
  write32(hdr+0x74, 0);
  // Handler
  write32(hdr+0x78, 0);
  // Resource
  write32(hdr+0x7C, 0xFFFFFFFF);
}

int cmd_wshead(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_inbin = NULL;
  const char *arg_outbin = NULL;

  Config cfg = {
    .ws_type = WsType_WS,
    .outfmt = FmtType_ROM,
    .romsize = -1,
    .mfgid = 0x00,
    .cartid = 0x00,
    .ver = 0x00,
    .save = 0x00,
    .lsi = 0x00,
    .flag = 0x04,
    .entrypoint = 0x20000000,
    .prgbank = 0,
    .title = "NONAME",
    .freya_fname = "NONAME",
  };

  int dst_idx = 0;
  for(int i = 0; i < argc; i++) {
    if(argv[i][0] != '-') {
      switch(dst_idx) {
        case 0: arg_inbin = argv[i]; break;
        case 1: arg_outbin = argv[i]; break;
      }
      dst_idx++;
      continue;
    }
    if(strcmp(argv[i], "-type") == 0) {
      i++;
      if(strcmp(argv[i], "ws") == 0) {
        cfg.ws_type = WsType_WS;
      }else if(strcmp(argv[i], "wsc") == 0) {
        cfg.ws_type = WsType_WSC;
      }
    }else if(strcmp(argv[i], "-fmt") == 0) {
      i++;
      if(strcmp(argv[i], "rom") == 0) {
        cfg.outfmt = FmtType_ROM;
      }else if(strcmp(argv[i], "wwsft") == 0) {
        cfg.outfmt = FmtType_WWSft;
      }else if(strcmp(argv[i], "freya") == 0) {
        cfg.outfmt = FmtType_Freya;
      }
    }else if(strcmp(argv[i], "-title") == 0) {
      cfg.title = argv[++i];
    }else if(strcmp(argv[i], "-fname") == 0) {
      cfg.freya_fname = argv[++i];
    }else if(strcmp(argv[i], "-mfgid") == 0) {
      cfg.mfgid = strtol(argv[++i], NULL, 0);
    }else if(strcmp(argv[i], "-cartid") == 0) {
      cfg.cartid = strtol(argv[++i], NULL, 0);
    }else if(strcmp(argv[i], "-ver") == 0) {
      cfg.ver = strtol(argv[++i], NULL, 0);
    }else if(strcmp(argv[i], "-sram") == 0) {
      i++;
      cfg.save &= ~0x0F;
      if(strcmp(argv[i], "none") == 0) {
        cfg.save |= 0x00;
      }else if(strcmp(argv[i], "256K") == 0) {
        cfg.save |= 0x01;
      }else if(strcmp(argv[i], "1M") == 0) {
        cfg.save |= 0x02;
      }else if(strcmp(argv[i], "2M") == 0) {
        cfg.save |= 0x03;
      }
    }else if(strcmp(argv[i], "-eeprom") == 0) {
      i++;
      cfg.save &= ~0xF0;
      if(strcmp(argv[i], "none") == 0) {
        cfg.save |= 0x00;
      }else if(strcmp(argv[i], "1K") == 0) {
        cfg.save |= 0x10;
      }else if(strcmp(argv[i], "2K") == 0) {
        cfg.save |= 0x20;
      }else if(strcmp(argv[i], "4K") == 0) {
        cfg.save |= 0x30;
      }else if(strcmp(argv[i], "8K") == 0) {
        cfg.save |= 0x40;
      }else if(strcmp(argv[i], "16K") == 0) {
        cfg.save |= 0x50;
      }else if(strcmp(argv[i], "32K") == 0) {
        cfg.save |= 0x60;
      }else if(strcmp(argv[i], "64K") == 0) {
        cfg.save |= 0x70;
      }else if(strcmp(argv[i], "128K") == 0) {
        cfg.save |= 0x80;
      }
    }else if(strcmp(argv[i], "-orient") == 0) {
      i++;
      if(strcmp(argv[i], "h") == 0) {
        cfg.flag &= ~1;
      }else if(strcmp(argv[i], "v") == 0) {
        cfg.flag |= 1;
      }
    }else if(strcmp(argv[i], "-romspeed") == 0) {
      i++;
      if(strcmp(argv[i], "fast") == 0) {
        cfg.flag &= ~8;
      }else if(strcmp(argv[i], "slow") == 0) {
        cfg.flag |= 8;
      }
    }else if(strcmp(argv[i], "-rombus") == 0) {
      switch(strtol(argv[++i], NULL, 0)) {
        case 8:
          cfg.flag &= ~4;
          break;
        case 16:
          cfg.flag |= 4;
          break;
      }
    }else if(strcmp(argv[i], "-romsize") == 0) {
      i++;
      if(strcmp(argv[i], "auto") == 0) {
        cfg.romsize = -1;
      }else if(strcmp(argv[i], "1M") == 0) {
        cfg.romsize = 0;
      }else if(strcmp(argv[i], "2M") == 0) {
        cfg.romsize = 1;
      }else if(strcmp(argv[i], "4M") == 0) {
        cfg.romsize = 2;
      }else if(strcmp(argv[i], "8M") == 0) {
        cfg.romsize = 3;
      }else if(strcmp(argv[i], "16M") == 0) {
        cfg.romsize = 4;
      }else if(strcmp(argv[i], "24M") == 0) {
        cfg.romsize = 5;
      }else if(strcmp(argv[i], "32M") == 0) {
        cfg.romsize = 6;
      }else if(strcmp(argv[i], "48M") == 0) {
        cfg.romsize = 7;
      }else if(strcmp(argv[i], "64M") == 0) {
        cfg.romsize = 8;
      }else if(strcmp(argv[i], "128M") == 0) {
        cfg.romsize = 9;
      }
    }else if(strcmp(argv[i], "-entrypoint") == 0) {
      cfg.entrypoint = strtol(argv[++i], NULL, 0);
    }else if(strcmp(argv[i], "-prgbank") == 0) {
      i++;
      if(strcmp(argv[i], "last") == 0) {
        cfg.prgbank = -1;
      }else{
        cfg.prgbank = strtol(argv[i], NULL, 0);
      }
    }else if(strcmp(argv[i], "-rtc") == 0) {
      cfg.lsi |= 1;
    }
  }

  FILE *fp = fopen(arg_inbin, "rb");
  if(fp == NULL)
    return 1;
  fseek(fp, 0, SEEK_END);
  size_t size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  size_t rombytes;
  rombytes = size + header_sizes[cfg.outfmt];
  if(cfg.outfmt == FmtType_Freya) {
    // Freya images are any size, but rounded up to 0x80
    rombytes = (rombytes + 0x7F) & ~0x7F;
  }else{
    if(cfg.romsize < 0) {
      if(rombytes > 128*128*1024) // 128Mbit is the max size
        rombytes = 128*128*1024;
      for(cfg.romsize = 0; rombytes > rom_size_tbl[cfg.romsize]; cfg.romsize++);
    }

    rombytes = rom_size_tbl[cfg.romsize];
    // WWSFT is always 3.5Mbit
    if(cfg.outfmt == FmtType_WWSft)
      rombytes = 448*1024;

    if(rombytes < size) size = rombytes;
  }

  // Allocate and clear the ROM space
  uint8_t *bin = malloc(rombytes);
  memset(bin, 0xFF, rombytes);
  fread(bin+binary_offsets[cfg.outfmt], size, 1, fp);
  fclose(fp);

  // Setup the header
  switch(cfg.outfmt) {
    case FmtType_ROM:
    case FmtType_WWSft:
      prv_add_rom_hdr(bin, rombytes, &cfg);
      break;
    case FmtType_Freya:
      prv_add_freya_hdr(bin, rombytes, &cfg);
      break;
  }

  fp = fopen(arg_outbin, "wb");
  if(fp == NULL)
    return 0;
  fwrite(bin, rombytes, 1, fp);
  fclose(fp);

  free(bin);

  return 0;
}
