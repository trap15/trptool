#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// in out th [x|y]

int cmd_pcxexp(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin = argv[0];
  const char *arg_pcxout = argv[1];
  const char *arg_th = argv[2];
  const char *arg_fliptype = argv[3];

  int fliptype = 0;
  while(*arg_fliptype) {
    switch(*arg_fliptype) {
      case 'x': case 'X': fliptype ^= 1; break;
      case 'y': case 'Y': fliptype ^= 2; break;
    }
    arg_fliptype++;
  }

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;

  size_t each_th = strtol(arg_th, NULL, 0);

  PcxFile opcx;
  opcx.bpp = pcx.bpp;
  opcx.w = pcx.w;
  opcx.h = (pcx.h * 2) - (each_th * 2);
  memcpy(opcx.pal16, pcx.pal16, sizeof(pcx.pal16));
  opcx.stride = opcx.w * opcx.bpp / 8;
  opcx.data = malloc(opcx.h * opcx.stride);
  opcx.pal = pcx.pal;

  memcpy(opcx.data, pcx.data, pcx.h * pcx.stride);
  size_t off = pcx.h * opcx.stride;
  size_t soff = (pcx.h - each_th) * pcx.stride;
  for(size_t y = pcx.h; y < opcx.h; y += each_th) {
    soff -= each_th * pcx.stride;
    for(size_t ty = 0; ty < each_th; ty++) {
      size_t oty = ty;
      if(fliptype & 2) {
        oty = each_th - ty - 1;
      }
      for(size_t x = 0; x < opcx.w; x++) {
        size_t ox = x;
        if(fliptype & 1) {
          ox = opcx.w - x - 1;
        }
        opcx.data[x+off] = pcx.data[soff+(oty*pcx.stride)+ox];
      }
      off += opcx.stride;
    }
  }

  pcx_save(&opcx, arg_pcxout);

  return 0;
}
