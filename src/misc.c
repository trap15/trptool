#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"

// Jenkins one-at-a-time hash function
uint32_t hash32(const uint8_t *data, size_t len) {
  uint32_t hash = 0;
  for(size_t i = 0; i < len; i++) {
    hash += data[i];
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

int align(int alignment, int value) {
  value += alignment - 1;
  return value - (value % alignment);
}
