#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"
#include "pcg.h"

typedef struct {
  uint8_t width;
} GlyphMeta;
typedef struct {
  uint8_t data[8];
} GlyphContent;

int cmd_fontmk(int argc, const char *argv[]) {
  const char *arg_pcxin = argv[0];
  const char *arg_outmeta = argv[1];
  const char *arg_outcont;
  if(argc > 2)
    arg_outcont = argv[2];
  else
    arg_outcont = arg_outmeta;

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;

  uint32_t bpp = 1;

  uint32_t max_w = 8;
  uint32_t max_h = 8;

  uint8_t bpp_lim = 1 << bpp;
  uint32_t glyphs_w = pcx.w / max_w;
  uint32_t glyphs_h = pcx.h / max_h;
  uint32_t glyphs = glyphs_w * glyphs_h;

  GlyphContent *glyph_content = calloc(sizeof(GlyphContent), glyphs);
  GlyphMeta *glyph_meta = calloc(sizeof(GlyphMeta), glyphs);

  uint32_t idx = 0;
  for(size_t y = 0; y < glyphs_h; y++) {
    for(size_t x = 0; x < glyphs_w; x++, idx++) {
      uint32_t px = x * max_w;
      uint32_t py = y * max_h;

      GlyphMeta *meta = glyph_meta+idx;
      GlyphContent *cont = glyph_content+idx;

      meta->width = 0;
      while(pcx.data[px+(py*pcx.stride)+meta->width] < bpp_lim) meta->width++;

      for(uint32_t iy = 0; iy < max_h; iy++) {
        cont->data[iy] = 0;
        for(uint32_t ix = 0; ix < max_w; ix++) {
          cont->data[iy] <<= bpp;
          uint8_t c = pcx.data[px+((py+iy)*pcx.stride)+ix];
          if(c < bpp_lim) {
            cont->data[iy] |= c;
          }
        }
      }
    }
  }

  FILE *metafp = fopen(arg_outmeta, "wb");
  FILE *contfp;
  if(strcmp(arg_outmeta, arg_outcont) == 0) {
    contfp = metafp;
  }else{
    contfp = fopen(arg_outcont, "wb");
  }
  fwrite(glyph_meta, sizeof(GlyphMeta), glyphs, metafp);
  fwrite(glyph_content, sizeof(GlyphContent), glyphs, contfp);
  if(contfp != metafp) {
    fclose(contfp);
  }
  fclose(metafp);

  return 0;
}
