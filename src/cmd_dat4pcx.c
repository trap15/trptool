#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// COMMAND_DEF(dat4pcx, 2, 2, "in.pcx out.dat")

#define FRAME_FLAG_ROTATE (1 << 0)
typedef struct TextureFrameData {
  double u[2], v[2];
  uint16_t w, h;
  uint16_t flag;
  uint16_t palette;
} TextureFrameData;

typedef struct TextureRegistryKey {
  uint32_t frame_count;
  TextureFrameData *frames;
} TextureRegistryKey;

typedef struct TextureRegistry {
  uint32_t dat_keys, dat_frames;
  TextureFrameData atlas_frame;
  uint32_t key_count;
  TextureRegistryKey *keys;
} TextureRegistry;

int cmd_dat4pcx(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin = argv[0];
  const char *arg_datout = argv[1];

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;

  TextureRegistry texreg = {
    .atlas_frame = {
      .u = {0.0, 1.0},
      .v = {0.0, 1.0},
      .w = pcx.w, .h = pcx.h,
      .flag = 0,
      .palette = 0,
    },
    .key_count = 1,
  };
  texreg.keys = calloc(texreg.key_count, sizeof(TextureRegistryKey));
  texreg.keys[0].frame_count++;
  texreg.keys[0].frames = calloc(texreg.keys[0].frame_count, sizeof(TextureFrameData));
  texreg.keys[0].frames[0] = texreg.atlas_frame;
  texreg.dat_keys = 1;
  texreg.dat_frames = 1;

  FILE *fp = fopen(arg_datout, "wb");
  void *ptr = NULL;
  TextureRegistry *reg = &texreg;
#define SERIAL_VAR(var) fwrite(&(var), sizeof(var), 1, fp);
#define SERIAL_OFFSET(var) do { ptr = (void*)(sizeof(void*)); SERIAL_VAR(ptr); } while(0)
  SERIAL_VAR(reg->dat_keys);
  SERIAL_VAR(reg->dat_frames);
  SERIAL_VAR(reg->atlas_frame);
  SERIAL_VAR(reg->key_count);
  SERIAL_OFFSET(reg->keys);
  for(uint32_t i = 0; i < reg->key_count; i++) {
    SERIAL_VAR(reg->keys[i].frame_count);
    SERIAL_OFFSET(reg->keys[i].frames);
  }
  for(uint32_t i = 0; i < reg->key_count; i++) {
    for(uint32_t l = 0; l < reg->keys[i].frame_count; l++) {
      SERIAL_VAR(reg->keys[i].frames[l]);
    }
  }
  fclose(fp);

  return 0;
}

