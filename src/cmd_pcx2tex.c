#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// in.pcx in.dat [0|1] out.tex

typedef struct TexHdr {
  uint16_t order1a_magic;
  uint16_t order1b_magic;
  uint32_t order2_magic;
  uint32_t dat_keys;
  uint32_t dat_frames;
  uint32_t w, h;
} TexHdr;

int cmd_pcx2tex(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin = argv[0];
  const char *arg_datin = argv[1];
  const char *arg_palok = argv[2];
  const char *arg_binout = argv[3];
  int palok = arg_palok[0] == '0' ? 0 : 1;

  PcxFile pcx;
  if(!pcx_new(&pcx, arg_pcxin))
    return 1;
  if(pcx.bpp != 8) return 1;

  FILE *ofp = fopen(arg_binout, "wb");
  TexHdr hdr = {
    .order1a_magic = 0xFFFE,
    .order1b_magic = 0xFDFC,
    .order2_magic = 0x01020304,
    .w = pcx.w,
    .h = pcx.h,
  };

  FILE *fp = fopen(arg_datin, "rb");
  fseek(fp, 0, SEEK_END);
  uint32_t dat_size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  fread(&hdr.dat_keys, 4, 1, fp);
  fread(&hdr.dat_frames, 4, 1, fp);
  dat_size -= 8;

#define SERIAL_VAR(var) fwrite(&(var), sizeof(var), 1, ofp);
  SERIAL_VAR(hdr.order1a_magic);
  SERIAL_VAR(hdr.order1b_magic);
  SERIAL_VAR(hdr.order2_magic);
  SERIAL_VAR(hdr.dat_keys);
  SERIAL_VAR(hdr.dat_frames);
  SERIAL_VAR(hdr.w);
  SERIAL_VAR(hdr.h);
  for(int y = 0; y < pcx.h; y++) {
    for(int x = 0; x < pcx.w; x++) {
      uint8_t byte = pcx.data[x + (y * pcx.stride)];
      uint8_t thispal = byte >> 4;
      uint8_t thisidx = byte & 0xF;

      (void)thispal;
      uint8_t v8 = palok ? byte : thisidx;
      fwrite(&v8, sizeof(v8), 1, ofp);
    }
  }
  while(dat_size) {
#define READBUF_SIZE 32
    uint8_t buf[READBUF_SIZE];
    uint32_t buf_size = READBUF_SIZE;
    if(buf_size > dat_size) buf_size = dat_size;
    fread(buf, buf_size, 1, fp);
    fwrite(buf, buf_size, 1, ofp);
    dat_size -= buf_size;
  }
  fclose(fp);
  fclose(ofp);
  return 0;
}
