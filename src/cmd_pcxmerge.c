#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "misc.h"
#include "pcx.h"
#include "format.h"

// in1 in2 out

int cmd_pcxmerge(int argc, const char *argv[]) {
  (void)argc;
  const char *arg_pcxin1 = argv[0];
  const char *arg_pcxin2 = argv[1];
  const char *arg_pcxout = argv[2];

  PcxFile pcx1;
  if(!pcx_new(&pcx1, arg_pcxin1))
    return 1;
  PcxFile pcx2;
  if(!pcx_new(&pcx2, arg_pcxin2))
    return 1;

  PcxFile opcx;
  opcx.bpp = pcx1.bpp;
  opcx.w = pcx1.w;
  opcx.h = pcx1.h + pcx2.h;
  memcpy(opcx.pal16, pcx1.pal16, sizeof(pcx1.pal16));
  opcx.stride = opcx.w * opcx.bpp / 8;
  opcx.data = malloc(opcx.h * opcx.stride);
  opcx.pal = pcx1.pal;

  memcpy(opcx.data, pcx1.data, pcx1.h * pcx1.stride);
  memcpy(opcx.data + (pcx1.h * pcx1.stride), pcx2.data, pcx2.h * pcx2.stride);

  pcx_save(&opcx, arg_pcxout);

  return 0;
}
